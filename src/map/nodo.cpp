/*
 * nodo.cpp
 *
 *  Created on: 26/11/2014
 *      Author: God
 */


#include "map/globalMap.hpp"
#include "map/nodo.hpp"

//Constructor
CNodo::CNodo() : coco(false), cocoG(false), visitado(false) {
	/**
	 * enum output_t
	 *
	 * hijo [ 0 ] arriba
	 * hijo [ 1 ] abajo
	 * hijo [ 2 ] derecha
	 * hijo [ 3 ] izquierda
	 *
	 */
	hijos = new CNodo*[HIJOS];
	for(int i = 0; i<HIJOS; i++){
		hijos[i] = NULL;
	}
}

CNodo::CNodo(bool c, bool cG) : coco(c), cocoG(cG), visitado(false) {
	/**
	 * enum output_t
	 *
	 * hijo [ 0 ] arriba
	 * hijo [ 1 ] abajo
	 * hijo [ 2 ] derecha
	 * hijo [ 3 ] izquierda
	 *
	 */
	hijos = new CNodo*[HIJOS];
	for(int i = 0; i<HIJOS; i++){
		hijos[i] = NULL;
	}
}

//Destructor
CNodo::~CNodo(){

	for(int i = 0; i<HIJOS; i++){
		hijos[i] = NULL;
	}

	delete [] hijos;

}

//Getters
bool CNodo::getCoco(){
	return coco;
}

bool CNodo::getCocoG(){
	return cocoG;
}

bool CNodo::getVisitado(){
	return visitado;
}
/*
CNodo* CNodo::getHijo(output_t direccion){
	return hijos[direccion];
}*/

CNodo* CNodo::getHijo(int direccion){
	return hijos[direccion];
}

//setters
void CNodo::setHijo(CNodo* next, output_t direccion){
	hijos[direccion] = next;
}

void CNodo::setVisitado(bool set){
	visitado = set;
}

void CNodo::setCoco(bool set){
	coco = set;
}
void CNodo::setCocoG(bool set){
	cocoG = set;
}

//funciones

void CNodo::comer(){
	coco = false;
	cocoG = false;
}

int CNodo::numeroHijos(){

	int num = 0;

	for(int i = 0; i<HIJOS; i++){
		if(hijos[i] != NULL && !hijos[i]->getVisitado()){
			num++;
		}
	}

	return num;
}
