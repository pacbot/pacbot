/*
 * globalMap.cpp
 *
 *  Created on: 26/11/2014
 *      Author: God
 */

#include "map/globalMap.hpp"

const char* NOMBRE_FICHERO = "assets/mapa.txt";
const int HIJOS = 4;
const int FUERA_VISION = 0;
const int GLOBAL = INT_MAX;
const int VISION = 20;

const int COCO = 10, COCOG = 50;

const char CASILLA_COCO = '*';
const char CASILLA_COCO_GRANDE = 'W';
const char CASILLA_VACIA = '-';

double conversor01(int x, int y){
	return 1.0 - (((double)y - 1.0) / (double)x);
}
