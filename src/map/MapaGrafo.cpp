/*
 * MapaGrafo.cpp
 *
 *  Created on: 26/11/2014
 *      Author: God
 */

#include "map/MapaGrafo.hpp"

MapaGrafo::MapaGrafo(){

  initGame();
}

MapaGrafo::~MapaGrafo(){

	for(int i = 0; i<alto; i++){
		for(int j = 0; j<ancho; j++){
			delete mapa[i][j];
		}
		delete [] mapa[i];
	}

	delete [] mapa;


}

//Mover la posicion actual a alguna direccion
void MapaGrafo::move(int x, int y){

  if (x >= 0 && x < alto && y >= 0 && y < ancho && mapa[x][y] != NULL){
    posActual = mapa[x][y];
    comer();
  }

}

//Rellena el mapa que ser� nuestro tablero de trabajo
void MapaGrafo::initGrafo(char** tablero){

	//Creacion de mapa virtual de nodos
	for(int i = 0; i<alto; i++){
		for(int j = 0; j<ancho; j++){

			//Creando nodo
			if(tablero[i][j] == CASILLA_VACIA){
				mapa[i][j] = CNodo::CrearNodo(CASILLA_VACIA);
			}
			else if(tablero[i][j] == CASILLA_COCO){
				mapa[i][j] = CNodo::CrearNodo(CASILLA_COCO);
			}
			else if(tablero[i][j] == CASILLA_COCO_GRANDE){
				mapa[i][j] = CNodo::CrearNodo(CASILLA_COCO_GRANDE);
			}
			else{
				mapa[i][j] = NULL;
			}

			if(mapa[i][j] != NULL){
				posActual = mapa[i][j];
			}
		}
	}

	//Unir los nodos creados
    unionNodos();

}

//Recorre el mapa uniendo nodos
void MapaGrafo::unionNodos(){

	//Recorrer nodos y unir los punteros
	for(int i = 0; i<alto; i++){
		for(int j = 0; j<ancho; j++){
			//mirar alrededor
			if(mapa[i][j] != NULL){
				//norte
				if(i-1 >= 0 && mapa[i-1][j] != NULL){
					mapa[i][j]->setHijo(mapa[i-1][j],arriba);
				}
				//sur
				if(i+1 < alto && mapa[i+1][j] != NULL){
					mapa[i][j]->setHijo(mapa[i+1][j],abajo);
				}
				//este
				if(j+1 < ancho && mapa[i][j+1] != NULL){
					mapa[i][j]->setHijo(mapa[i][j+1],derecha);
				}
				//oeste
				if(j-1 >= 0 && mapa[i][j-1] != NULL){
					mapa[i][j]->setHijo(mapa[i][j-1],izquierda);
				}
			}

		}
	}

	mapa[13][0]->setHijo(mapa[13][25], izquierda); // TODO Cableado: agujero de gusano
	mapa[13][25]->setHijo(mapa[13][0], derecha); // TODO Cableado: agujero de gusano
}

void MapaGrafo::initGame(){
    puntuacion = 0;

    ifstream is(NOMBRE_FICHERO);

    if(is.fail()){
      cerr << "Error al abrir el archivo." <<endl;
    }

    //tablero del mapa
    char** tablero;

    is >> ancho;
    is >> alto;

    //Tablero auxiliar que nos permitira mapearlo como si fuera un grafo
    tablero = new char*[alto];
    for(int i = 0; i<alto; i++){
      tablero[i] = new char[ancho];
    }

    numNodos = 0;

    //Tablero y numero de nodos que tendra nuestro grafo
    for(int i = 0; i<alto; i++){
      for(int j = 0; j<ancho; j++){
        is >> tablero[i][j];
        if(tablero[i][j] == CASILLA_VACIA || tablero[i][j] == CASILLA_COCO || tablero[i][j] == CASILLA_COCO_GRANDE){
          numNodos++;
        }
      }
    }

    //Nodos mapa
    mapa = new CNodo**[alto];
    for(int i = 0; i<alto; i++){
      mapa[i] = new CNodo*[ancho];
    }

      //Creando grafo del mapa

    initGrafo(tablero);

    for(int i = 0; i<alto; i++){
      delete [] tablero[i];
    }

    delete [] tablero;

    posActual = mapa[22][13]; // TODO Cableado
}


//Devuelve el nodo de la casilla i,j del tablero
CNodo* MapaGrafo::getPos(int i, int j){
	if( i < 0 || j < 0 || i >= alto || j >= ancho ){
		//cerr << "Destino fuera del mapa." << endl;
		return NULL;
	}
	return mapa[i][j];
}

int MapaGrafo::recorridoAmplitud(output_t dir, Objetivo obj){

	int VISION_MAX = 0;

	//camino no existente
	if(posActual->getHijo(dir) == NULL){
		return FUERA_VISION;
	}

	//actualizar vision del bot
	if(obj == coco){
		VISION_MAX = GLOBAL;
	}
	else{
		VISION_MAX = VISION;
	}



	CNodo* controlador = posActual->getHijo(dir);
	vector<CNodo*> camino;
	int distancia = 1, introducidos = 0, ramasPadre = 0, ramasActual = 0;
	bool encontrado = false;

	camino.push_back(controlador);

	posActual->setVisitado(true);

	ramasPadre = controlador->numeroHijos();

	while(!camino.empty() && !encontrado && distancia <= VISION_MAX){

		encontrado = buscar(camino[0],obj);

		if(!encontrado){
			//Ramas del nodo padre y avanzar un nivel
			if(ramasPadre == 0){
				ramasPadre = ramasActual;
				ramasActual = 0;
				distancia++;
			}

			//Agregar hijos
			for(int i = 0; i<HIJOS; i++){
				if(camino[0]->getHijo(i) && !camino[0]->getHijo(i)->getVisitado()){
					camino.push_back(camino[0]->getHijo(i));
					introducidos++;
				}
			}

			//Nodo visitado
			camino[0]->setVisitado(true);

			//Borrar nodo del principio de la cola
			camino.erase(camino.begin());

			//Actualizar ramas actuales
			ramasActual += introducidos;

			introducidos = 0;
			ramasPadre--;

		}
		else{

      if(ramasPadre == 0){
        distancia++;
      }

			limpiarVisitados();
			return distancia;
		}

	}

	limpiarVisitados();
	return FUERA_VISION;
}

int MapaGrafo::recorridoAmplitud(output_t dir, int x, int y){

	int VISION_MAX = VISION;
	CNodo* controlador = posActual->getHijo(dir);
	CNodo* nodoFinal = getPos(x,y);
	vector<CNodo*> camino;
	int distancia = 1, introducidos = 0, ramasPadre = 0, ramasActual = 0;
	bool encontrado = false;

	//camino no existente
	if(controlador == NULL || nodoFinal == NULL){
		return FUERA_VISION;
	}

	camino.push_back(controlador);

	posActual->setVisitado(true);

	ramasPadre = controlador->numeroHijos();

	while(!camino.empty() && !encontrado && distancia <= VISION_MAX){

		if(nodoFinal == camino[0]){
			encontrado = true;

      if(ramasPadre == 0){
        distancia++;
      }

			limpiarVisitados();
			return distancia;
		}

		if(!encontrado){
			//Ramas del nodo padre y avanzar un nivel
			if(ramasPadre == 0){
				ramasPadre = ramasActual;
				ramasActual = 0;
				distancia++;
			}

			//Agregar hijos
			for(int i = 0; i<HIJOS; i++){
				if(camino[0]->getHijo(i) && !camino[0]->getHijo(i)->getVisitado()){
					camino.push_back(camino[0]->getHijo(i));
					introducidos++;
				}
			}

			//Nodo visitado
			camino[0]->setVisitado(true);

			//Borrar nodo del principio de la cola
			camino.erase(camino.begin());

			//Actualizar ramas actuales
			ramasActual += introducidos;

			introducidos = 0;
			ramasPadre--;

		}

	}

	limpiarVisitados();
	return FUERA_VISION;
}


double MapaGrafo::cocoCercano(output_t dir){
	double distPonderada = 0;
	double distMax = 0;
	double actual = recorridoAmplitud(dir,coco);

	if(actual == 0 || actual == 1){
		return actual;
	}

	int aux = 0;
	for(int i = 0; i<HIJOS; i++){
		aux = recorridoAmplitud((output_t)i,coco);
		if(distMax < aux )
			distMax = aux;
	}

	//regla de tres inversa
	distPonderada = conversor01(distMax,actual);

	return distPonderada;
}

double MapaGrafo::cocoGCercano(output_t dir){
	double distPonderada = 0;
	//int distMax = 0;
	int actual = recorridoAmplitud(dir,cocoGrande);

	if(actual == 0){
		return actual;
	}
/*
	int aux = 0;
	for(int i = 0; i<HIJOS; i++){
		aux = recorridoAmplitud((output_t)i,cocoGrande);
		if(distMax < aux )
			distMax = aux;
	}
*/

	//regla de tres inversa
	distPonderada = conversor01(VISION,actual);

	return distPonderada;
}


double MapaGrafo::fantasmaCercano(int x,int y,output_t dir){
	double distPonderada = 0;
	//double distMax = 0;

	int actual = recorridoAmplitud(dir,x,y);

	if(actual == 0){
		return actual;
	}

/*
	int aux = 0;
	for(int i = 0; i<HIJOS; i++){
		aux = recorridoAmplitud((output_t)i,x,y);
		if(distMax < aux )
			distMax = aux;
	}
*/
	//regla de tres inversa
	distPonderada = conversor01(VISION,actual);

	return distPonderada;
}

bool MapaGrafo::movimientoPermitido(output_t dir){

  return (posActual->getHijo(dir) != NULL);
}

//funciones de ayuda
/*
bool MapaGrafo::visitado(CNodo* nodo, vector<CNodo*> visitados){

	bool existe = false;

	for(int i = 0; i<visitados.size(); i++){
		if(visitados[i] == nodo){
			existe = true;
		}
	}

	return existe;

}*/

bool MapaGrafo::buscar(CNodo* nodo, Objetivo obj){
	if(obj == coco){
		return nodo->getCoco();
	}
	else if(obj == cocoGrande){
		return nodo->getCocoG();
	}
	else{
		//cout << "No tiene coco !!!!" <<endl;
		return false;
	}
}

void MapaGrafo::limpiarVisitados(int x, int y){

  if (x < 0 || y < 0 || y >= ancho || x >= alto)
    return;

  mapa[x][y]->setVisitado(false);
  for (int i = -1; i <= 1; i += 2) {

    if (mapa[x + i][y] != NULL && mapa[x + i][y]->getVisitado())
      limpiarVisitados(x + i, y);

    if (mapa[x][y + 1] != NULL && mapa[x][y + i]->getVisitado())
      limpiarVisitados(x, y + i);
  }

  if (x == 13) {

    if (y == 0 && mapa[13][25]->getVisitado())
      limpiarVisitados(13,25);

    if (y == 25 && mapa[13][0]->getVisitado())
      limpiarVisitados(13,0);
  }
}

void MapaGrafo::limpiarVisitados(){

  for (int i = 0; i < alto; i++) {
    for (int j = 0; j < ancho; j++) {

      if (mapa[i][j] != NULL && mapa[i][j]->getVisitado()) {
        mapa[i][j]->setVisitado(false);
      }
    }
  }
}

void MapaGrafo::comer(){
  if(posActual->getCoco()){
    puntuacion += COCO;
    posActual->setCoco(false);
  }
  else if(posActual->getCocoG()){
    puntuacion += COCOG;
    posActual->setCocoG(false);
  }
}

