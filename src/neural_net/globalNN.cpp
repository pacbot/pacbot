/*
 * globalNN.cpp
 *
 *  Created on: 24/11/2014
 *      Author: Cristo
 */

#include "neural_net/globalNN.hpp"

const int DECIMALS = 2;

const extern int SIZE_FILE_NAME = 50, SIZE_FLOAT = 30;

const weight_t LOWER_WEIGHT = -1.0, HIGHER_WEIGHT = 1.0;
const int N_HIDDEN_LAYERS = 1;
const int N_NEURON_LAYERS = N_HIDDEN_LAYERS + 2;
const int N_WEIGHTS_LAYERS = N_HIDDEN_LAYERS + 1;
const int N_NEURONS[] = {32, 11, 4}; // {n, h, m} -> h = sqrt(m * n)

const extern neuron_t ALFA = 0.2;

/**
 * @brief Generar pesos aleatorios flotantes entre dos valores
 * @param first Peso mínimo
 * @param second Peso máximo
 * @see Se genera un peso aleatorio flotante entre dos valores dados (ambos incluidos), con DECIMALS decimales de precisión
 */
weight_t randWeightBetween(weight_t first, weight_t second) {

  return (rand() % (int)((second - first) * pow(10, DECIMALS) + 1)) / pow(10, DECIMALS) + first;
}

/**
 * @brief Función sigmoide
 * @param x Valor de 'x'
 * @return 'y', valor de la función para el valor de 'x' dado
 */
neuron_t sigmoid(neuron_t x) {

  return 1.0 / (1.0 + pow(M_E,-x));
}

/**
 * @brief Derivada de la función sigmoide
 * @param x Valor de 'x'
 * @return 'y', valor de la función para el valor de 'x' dado
 */
neuron_t derivative_sigmoid(neuron_t x) {

  neuron_t e_x = pow(M_E, x);

  return  e_x / pow((e_x + 1.0), 2);
}

/**
 * @brief Función identidad
 * @param x Valor de 'x'
 * @return 'y', valor de la función para el valor de 'x' dado
 */
neuron_t identity(neuron_t x) {

  return x;
}
