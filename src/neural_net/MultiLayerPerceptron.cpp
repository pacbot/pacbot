/*
 * MultiLayerPerceptron.cpp
 *
 *  Created on: 24/11/2014
 *      Author: Cristo
 */

#include "neural_net/MultiLayerPerceptron.hpp"

/**
 * @brief Constructor por defecto que reserva memoria para las capas de neuronas y las matrices de pesos de las sinapsis
 * @see N_NEURON_LAYERS es el nº de capas y N_WEIGHTS_LAYERS el de matrices de pesos, valores calculados
 * en función de N_HIDDEN_LAYERS, el nº de capas ocultas. N_NEURONS determina el nº de neuronas de cada capa
 */
MultiLayerPerceptron::MultiLayerPerceptron(): layers(NULL), weights(NULL) {
  
  layers = new NeuronLayer*[N_NEURON_LAYERS];
  weights = new WeightMatrix*[N_WEIGHTS_LAYERS];

  for (int n = 0; n < N_NEURON_LAYERS; ++n)
    layers[n] = new NeuronLayer(N_NEURONS[n]);

  for (int w = 0; w < N_WEIGHTS_LAYERS; ++w)
    weights[w] = new WeightMatrix(N_NEURONS[w], N_NEURONS[w + 1]);
}

/**
 * @brief Destructor por defecto que libera la memoria de las capas de neuronas y las matrices de pesos de las sinapsis
 */
MultiLayerPerceptron::~MultiLayerPerceptron() {

  if (layers != NULL) {
    for (int n = 0; n < N_NEURON_LAYERS; ++n)
      if (layers[n] != NULL)
        delete layers[n];
    delete [] layers;
  }

  if (weights != NULL) {
    for (int w = 0; w < N_WEIGHTS_LAYERS; ++w)
      if (weights[w] != NULL)
        delete weights[w];
    delete [] weights;
  }
}

/**
 * @brief Leer de fichero los valores de entrada de la red
 * @param file Nombre del fichero que contiene los datos
 * @see N_NEURONS[0] es el nº de neuronas de la primera capa, y por tanto el nº de valores a leer
 * @warning La función consume la entrada, es decir, se liberará la memoria del vector de entrada una vez haya sido utilizado
 */
void MultiLayerPerceptron::readInput(char* file) {

  fstream is;
  is.open(file, ios_base::in);

  if(is.is_open()) {

    neuron_t* input = new neuron_t[N_NEURONS[0]];

    string content((istreambuf_iterator<char>(is)), istreambuf_iterator<char>());
    char* text = new char[content.size() + 1];
    strcpy(text, content.c_str());

    xml_document<> doc;
    doc.parse<0>(text);

    int n_input = 0;
    traverseInputTree(doc.first_node("Input"), input, n_input);

    is.close();

    delete [] text;

    setInput(input);
  }
  else
    cerr << "Error: Lectura incorrecta del fichero " << file << endl;

  delete [] file;
}

/**
 * @brief Asignar los valores de entrada a la red
 * @param input Vector con los valores de entrada de la red
 * @see N_NEURONS[0] es el nº de neuronas de la primera capa, y por tanto el tamaño del vector proporcionado
 * @warning La función consume la entrada, es decir, se liberará la memoria del vector de entrada una vez haya sido utilizado
 */
void MultiLayerPerceptron::setInput(neuron_t* input) {

  layers[0]->setInput(input);
}

/**
 * @brief Inicializar la información (pesos) de la red a partir de fichero
 * @param file Nombre del fichero que contiene los datos
 * @warning La función consume la entrada, es decir, se liberará la memoria del vector de entrada una vez haya sido utilizado
 */
void MultiLayerPerceptron::importNet(char* file) {

  weight_t*** weights_layers;

  fstream is;
  is.open(file, ios_base::in);

  if(is.is_open()) {

    string content((istreambuf_iterator<char>(is)), istreambuf_iterator<char>());
    char* text = new char[content.size() + 1];
    strcpy(text, content.c_str());

    xml_document<> doc;
    doc.parse<0>(text);

    weights_layers = new weight_t**[N_WEIGHTS_LAYERS];
    xml_node<>* root = doc.first_node("Neural_Net");

    int l = 0;
    for (xml_node<>* n_m = root->first_node("Weight_Matrix"); n_m; n_m = n_m->next_sibling("Weight_Matrix")) {

      weights_layers[l] = new weight_t*[N_NEURONS[l]];

      int ni = 0;
      for (xml_node<>* n_r = n_m->first_node("Input_Neuron"); n_r; n_r = n_r->next_sibling("Input_Neuron")) {

        weights_layers[l][ni] = new weight_t[N_NEURONS[l + 1]];

        int no = 0;
        for (xml_node<>* n_c = n_r->first_node("Output_Neuron"); n_c; n_c = n_c->next_sibling("Output_Neuron")) {

          weights_layers[l][ni][no] = atof(n_c->value());
          ++no;
        }
        ++ni;
      }
      ++l;
    }

    delete [] text;

    is.close();

    loadWeights(weights_layers);
  }
  else
      cerr << "Error: Lectura incorrecta del fichero " << file << endl;

  delete [] file;
}

/**
 * @brief Inicializar los pesos de la red
 * @param weights_layers Vector de matrices de pesos
 * @see El tamaño del vector de matrices será igual a N_NEURON_LAYERS - 1, llamando al método WeightMatrix::initWeights(weight_t**)
 * para inicializar cada matriz de pesos
 * @warning La función consume la entrada, es decir, se liberará la memoria no necesaria del vector de entrada una vez haya sido utilizado;
 * además, el resto de la memoria (matrices de pesos) se asigna sin realizar copia, de modo que no se debe liberar la memoria; de dicha gestión
 * se encarga la propia clase
 */
void MultiLayerPerceptron::loadWeights(weight_t*** weights_layers) {

  for (int w = 0; w < N_WEIGHTS_LAYERS; ++w)
    weights[w]->initWeights(weights_layers[w]);
  delete [] weights_layers;
}

/**
 * @brief Inicializar los pesos de la red de forma aleatoria
 * @see WeightMatrix::initWeightsRandom(), método que inicializa aleatoriamente cada una de las matrices de pesos
 */
void MultiLayerPerceptron::initWeightsRandom() {

  for (int w = 0; w < N_WEIGHTS_LAYERS; ++w)
    weights[w]->initWeightsRandom();
}

/**
 * @brief Exportar a fichero la información (pesos) de la red neuronal
 * @param file Nombre del fichero al que exportar la información
 */
void MultiLayerPerceptron::exportNet(char* file) {

  fstream os;
  os.open(file, ios_base::out);

  if(os.is_open()) {

    xml_document<> doc;
    xml_node<>* root = doc.allocate_node(node_element, "Neural_Net");

    for (int l = 0; l < N_WEIGHTS_LAYERS; ++l) {

      xml_node<>* node_matrix = doc.allocate_node(node_element, "Weight_Matrix");

      for (int ni = 0; ni < N_NEURONS[l]; ++ni) {

        weight_t* output_weights = weights[l]->getOutputWeights(ni);
        xml_node<>* node_input = doc.allocate_node(node_element, "Input_Neuron");

        for (int no = 0; no < N_NEURONS[l + 1]; ++no) {

          char* num_str = new char[SIZE_FLOAT];
          sprintf(num_str, "%f", output_weights[no]);
          xml_node<>* node_output = doc.allocate_node(node_element, "Output_Neuron", num_str);
          node_input->append_node(node_output);
        }

        delete [] output_weights;
        node_matrix->append_node(node_input);
      }

      root->append_node(node_matrix);
    }

    doc.append_node(root);

    os << doc;

    os.close();
  }
  else
      cerr << "Error: Escritura incorrecta del fichero " << file << endl;

  delete [] file;
}

/**
 * @brief Computar la salida de la red neuronal
 * @warning Es necesario haber cargado previamente una entrada en la red, para que funcione correctamente
 * @see El método MultiLayerPerceptron::computeLayerInput(int) calcula la entrada de cada capa a partir de la capa anterior
 * y la matriz de pesos que las conecta, asignándola con el método NeuronLayer::setInput(neuron_t*); tras lo que se ejecuta
 * NeuronLayer::computeOutput(), función que se encarga de computar la salida de cada capa
 *
 * Se computa la salida de la red neuronal, propagando desde la capa de entrada hacia adelante, computando la salida
 * de todas las capas de neuronas a partir de la salida a anterior y la matriz de pesos que las conecta
 */
void MultiLayerPerceptron::computeOutput() {

  layers[0]->computeOutput(true);

  for (int l = 1; l < N_NEURON_LAYERS; ++l) {

    layers[l]->setInput(computeLayerInput(l));
    layers[l]->computeOutput();
  }
}

/**
 * @brief Obtener la salida de la red neuronal
 * @return Vector con la salida de la red neuronal
 * @warning Se debe proporcionar una entrada y computar la salida antes de solicitarla
 *
 * Se obtiene la salida de la red neuronal, es decir, la salida de la última capa de la red neuronal
 */
neuron_t* MultiLayerPerceptron::getOutput() {

  return layers[N_NEURON_LAYERS - 1]->getOutput();
}

/**
 * @brief Obtener el movimiento válido con mayor valor
 * @return Mejor movimiento válido
 * @see El movimiento se representa mediante el tipo output_t
 * @warning Se debe proporcionar una entrada y computar la salida antes de solicitar el mejor movimiento
 *
 * Se obtiene el movimiento válido (cada uno representado con cuatro salidas de la red), tal que el valor de
 * la salida que lo representa, es mayor que cualquiera de los asociados a un movimiento válido. Las cuatro primeras
 * entradas de la red son las que determinan si cada uno de los movimientos (respectivamente) son válidos (valor 1.0)
 * o no (valor 0.0)
 */
output_t MultiLayerPerceptron::getBetterMovement() {

  neuron_t* output = getOutput();
  neuron_t* net_input = layers[0]->getOutput();
  output_t movement = (output_t) 4;

  neuron_t max_value = -1.0;
  for (int i = 0; i < N_NEURONS[N_NEURON_LAYERS - 1]; ++i)
    if (output[i] >= max_value && net_input[i] == 1.0) {
      max_value = output[i];
      movement = (output_t) i;
    }

  if (movement == error)
    cerr << "Error: Movimiento no determinado correctamente" << endl;

  delete [] output;
  delete [] net_input;

  return movement;
}

/**
 * @brief Entrenar la red a partir de una muestra
 * @param target_output Vector con la salida deseada
 * @see Para el entrenamiento se computa la sensibilidad de todas las capas de la red (empezando por el final) con
 * MultiLayerPerceptron::computeNetSensitivity(neuron_t*), y a continuación se ajustan los pesos con MultiLayerPerceptron::adjustWeights()
 * @see El valor de 'alfa' utilizado para el entrenamiento lo indica ALFA que determinará la repercusión de la muestra proporcionada, en el
 * ajuste de pesos de la red
 * @warning Se debe haber proporcionado previamente una entrada y computado la salida de la red para realizar un entrenamiento
 * @warning La función consume la entrada, es decir, se liberará la memoria del vector de entrada una vez haya sido utilizado
 * @note Tras el entrenamiento sólo se modifican los pesos de la red, no se recalcula la salida real de la misma
 * @note El algoritmo de aprendizaje supervisado que se ejecuta es el de propagación hacia atrás (backpropagation)
 *
 * Se realiza el entrenamiento de la red, modificando los pesos, a partir del error entre la salida real y la esperada (la muestra)
 */
void MultiLayerPerceptron::simpleTraining(neuron_t* target_output) {

  computeNetSensitivity(target_output);
  adjustWeights();
}

/**
 * @brief Entrenar la red a partir de una serie de muestras leidas de fichero
 * @param training_file Nombre del fichero que contiene las muestras de entrenamiento (entradas y salidas)
 * @param weights_file Nombre del fichero que contiene los pesos de las sinapsis de la red (matrices de pesos). Si no se proporcionara
 * este parámetro, los pesos se inicializarían de forma aleatoria
 * @see El valor de 'alfa' utilizado para el entrenamiento lo indica ALFA que determinará la repercusión de cada muestra proporcionada en el
 * ajuste de pesos de la red
 * @warning La función consume la entrada, es decir, se liberará la memoria de cualquier vector de entrada una vez haya sido utilizado
 * @note El algoritmo de aprendizaje supervisado que se ejecuta es el de propagación hacia atrás (backpropagation)
 *
 * Se realiza el entrenamiento de la red, modificando los pesos, a partir del error entre la salida real y la esperada (la muestra)
 */
void MultiLayerPerceptron::completeTraining(char* training_file, char* weights_file) {

  neuron_t* input;
  neuron_t* output;

  if (weights_file != NULL)
    importNet(weights_file);
  else
    initWeightsRandom();

  fstream is;
  is.open(training_file, ios_base::in);

  if(is.is_open()) {

    xml_document<> doc;

    string content((istreambuf_iterator<char>(is)), istreambuf_iterator<char>());
    char* text = new char[content.size() + 1];
    strcpy(text, content.c_str());

    doc.parse<0>(text);

    for (xml_node<>* p = doc.first_node("Training")->first_node("Pattern"); p; p = p->next_sibling("Pattern")) {

      input = new neuron_t[N_NEURONS[0]];
      output = new neuron_t[N_NEURONS[N_NEURON_LAYERS - 1]];

      int n_input = 0;
      traverseInputTree(p->first_node("Input"), input, n_input);

      int o = 0;
      for (xml_node<>* o_n = p->first_node("Output")->first_node(); o_n; o_n = o_n->next_sibling()) {
        output[o] = atof(o_n->value());
        ++o;
      }

      setInput(input);
      computeOutput();
      simpleTraining(output);
    }

    delete [] text;

    is.close();
  }
  else
    cerr << "Error: Lectura incorrecta del fichero " << training_file << endl;

  delete [] training_file;
}



/**
 * @brief Recorrer recursivamente el árbol que contiene la entrada
 * @param node Puntero al nodo del árbol a recorrer
 * @param input Vector que contiene la entrada
 * @param n_input Índice de la siguiente entrada a leer
 * @see Se utiliza en la función readInput(char*)
 *
 * Se recorre el nodo actual, llamando recursivamente a todos los nodos hijos si los hubiera; o en
 * caso contrario, leyendo (y asignando) el valor de las correspondientes entradas
 */
void MultiLayerPerceptron::traverseInputTree(xml_node<>* node, neuron_t *input, int &n_input) {

  if (node->first_node() == 0) {
    input[n_input] = atof(node->value());
    ++n_input;
  }
  else
    for (xml_node<>* n = node->first_node(); n; n = n->next_sibling())
      traverseInputTree(n, input, n_input);
}

/**
 * @brief Computar la entrada de una capa de la red
 * @param n_layer Nº de la capa cuya entrada se desea calcular
 * @return Vector con la entrada calculada de la capa indicada
 * @warning La capa cuya entrada se desea calcular, debe ser una capa oculta o la de salida
 *
 * Se computa la entrada neta de las neuronas de la capa indicada de la red (oculta o de salida), a partir de las salidas
 * de las neuronas de la capa anterior y los pesos de las sinapsis que unen ambas capas
 */
neuron_t* MultiLayerPerceptron::computeLayerInput(int n_layer) {

  neuron_t* input = new neuron_t[N_NEURONS[n_layer]];
  neuron_t* layer_outputs = NULL;
  weight_t* in_weights = NULL;

  for (int n = 0; n < N_NEURONS[n_layer]; ++n) {

    input[n] = 0.0;
    layer_outputs = layers[n_layer - 1]->getOutput();
    in_weights = weights[n_layer - 1]->getInputWeights(n);

    for (int c = 0; c < N_NEURONS[n_layer - 1]; ++c)
      input[n] += in_weights[c] * layer_outputs[c];

    delete [] layer_outputs;
    delete [] in_weights;
  }

  return input;
}

/**
 * @brief Computar la sensibilidad de las neuronas de cada capa de la red
 * @param pattern Vector que contiene la salida esperada de la red
 * @note El algoritmo de aprendizaje supervisado en el que se utiliza es el de propagación hacia atrás (backpropagation)
 * @warning La función consume la entrada, es decir, se liberará la memoria del vector de entrada una vez haya sido utilizado
 *
 * Se computa la sensibilidad de las neuronas de todas las capas de la red, entendida como la medida del error de cada neurona
 * (determinado a partir de la relación entre la salida real y la esperada), y utilizada para el entrenamiento de la red
 */
void MultiLayerPerceptron::computeNetSensitivity(neuron_t* pattern) {

  neuron_t* summation;
  neuron_t* sensitivity;
  weight_t* weight;

  layers[N_NEURON_LAYERS - 1]->computeOutputLayerSensitivity(pattern);

  for (int l = N_NEURON_LAYERS - 2; l > 0; --l) {

    summation = new neuron_t[N_NEURONS[l]];
    sensitivity = layers[l + 1]->getSensitivity();

    for (int n = 0; n < N_NEURONS[l]; ++n) {

      summation[n] = 0.0;
      weight = weights[l]->getOutputWeights(n);

      for (int k = 0; k < N_NEURONS[l + 1]; ++k) {

        summation[n] += sensitivity[k] * weight[k];
      }

      delete [] weight;
    }

    layers[l]->computeHiddenLayerSensitivity(summation);
    delete [] sensitivity;
  }
}

/**
 * @brief Ajuste de los pesos de las sinapsis de la red neuronal
 * @see El valor de 'alfa' utilizado para el entrenamiento lo indica ALFA que determinará la repercusión de cada muestra proporcionada en el
 * ajuste de pesos de la red
 * @note El algoritmo de aprendizaje supervisado en el que se utiliza es el de propagación hacia atrás (backpropagation)
 *
 * Se realiza el ajuste de pesos de la red, como parte final del algoritmo de aprendizaje supervisado, a partir de las sensibilidades de las
 * neuronas de cada capa (excepto de la de entrada), previamente calculadas, relacionándolas con las salidas de la capa anterior
 */
void MultiLayerPerceptron::adjustWeights() {

  neuron_t* sensitivity;
  neuron_t* output;

  for (int w = 0; w < N_WEIGHTS_LAYERS; ++w) {

    sensitivity = layers[w + 1]->getSensitivity();
    output = layers[w]->getOutput();

    for (int ni = 0; ni < N_NEURONS[w]; ++ni)
      for (int no = 0; no < N_NEURONS[w + 1]; ++no)
        weights[w]->adjustWeight(ni, no, -ALFA * sensitivity[no] * output[ni]);

    delete [] sensitivity;
    delete [] output;
  }

}

void MultiLayerPerceptron::loadWeights(WeightMatrix** weight_layer) {

  if (weights != NULL) {
    for (int w = 0; w < N_WEIGHTS_LAYERS; ++w)
      if (weights[w] != NULL)
        delete weights[w];
    delete [] weights;
  }

  weights = weight_layer;
}

void MultiLayerPerceptron::mutateNet(int min_mutaciones, int max_mutaciones) {

  int mutaciones = randBetween(min_mutaciones, max_mutaciones);

  for (int i = 0; i < mutaciones; ++i) {

    int capa = randBetween(0, N_WEIGHTS_LAYERS - 1);
    weights[capa]->mutateWeight(randBetween(0, N_NEURONS[capa] - 1), randBetween(0, N_NEURONS[capa + 1] - 1));
  }
}

