/*
 * NeuronLayer.cpp
 *
 *  Created on: 24/11/2014
 *      Author: Cristo
 */

#include "neural_net/NeuronLayer.hpp"

/**
 * @brief Constructor
 * @param size Tamaño de la capa de neuronas
 *
 * Constructor en el que se establece el tamaño de la capa y se crea una cantidad equivalente de neuronas
 */
NeuronLayer::NeuronLayer(int size): lSize(size) {
  
  layer = new Neuron[size];
}

/**
 * @brief Destructor por defecto
 *
 * Liberación de la memoria reservada para las neuronas de la capa
 */
NeuronLayer::~NeuronLayer() {

  delete [] layer;
}

/**
 * @brief Obtener las salidas de las neuronas
 * @return Vector con las salidas de las neuronas de la capa
 * @note La información devuelta es una copia de la original, se puede liberar la memoria sin problema
 */
neuron_t* NeuronLayer::getOutput() {

  neuron_t* output = new neuron_t[lSize];
  for (int n = 0; n < lSize; ++n)
    output[n] = layer[n].getOutputValue();

  return output;
}

/**
 * @brief Obtener las sensibilidades de las neuronas
 * @return Vector con las sensibilidades de las neuronas de la capa
 * @note La información devuelta es una copia de la original, se puede liberar la memoria sin problema
 * @note Se emplea para el algoritmo de aprendizaje supervisado de propagación hacia atrás (backpropagation)
 */
neuron_t* NeuronLayer::getSensitivity() {

  neuron_t* sensitivity = new neuron_t[lSize];
  for (int n = 0; n < lSize; ++n)
    sensitivity[n] = layer[n].getSensitivity();

  return sensitivity;
}

/**
 * @brief Asignar valores de entrada a las neuronas
 * @param value Vector con los valores de entrada
 * @warning La función consume la entrada, es decir, se liberará la memoria del vector de entrada una vez haya sido utilizado
 *
 * Asignación a cada una de las neuronas de la capa, el valor del vector de entrada correspondiente a su posición
 */
void NeuronLayer::setInput(neuron_t* value) {

  for (int n = 0; n < lSize; ++n)
    layer[n].init(value[n]);
  delete [] value;
}

/**
 * @brief Computar salida de las neuronas de la capa
 * @param first_layer Indica si se trata de la capa de entrada
 *
 * Se computa la salida de cada una de las neuronas de la capa, indicando si se trata o no de neuronas de la capa
 * de entrada, en cuyo caso la computación es diferente
 */
void NeuronLayer::computeOutput(bool first_layer) {

  for (int n = 0; n < lSize; ++n)
    layer[n].computeOutput(first_layer);
}

/**
 * @brief Computar la sensibilidad de las neuronas de la capa de salida
 * @param pattern Vector con las salidas deseadas de la capa de salida
 * @warning La función consume la entrada, es decir, se liberará la memoria del vector de entrada una vez haya sido utilizado
 * @note Se emplea para el algoritmo de aprendizaje supervisado de propagación hacia atrás (backpropagation)
 *
 * Se computa la sensibilidad de cada una de las neuronas, asumiendo que se trata de la capa de salida, a partir de los
 * valores de salida deseados
 */
void NeuronLayer::computeOutputLayerSensitivity(neuron_t* pattern) {

  for (int n = 0; n < lSize; ++n)
    layer[n].computeOutputSensitivity(pattern[n]);
  delete [] pattern;
}

/**
 * @brief Computar la sensibilidad de las neuronas de la capa oculta
 * @param summation Vector con los sumatorios de pesos y sensibilidad de la capa siguiente
 * @warning La función consume la entrada, es decir, se liberará la memoria del vector de entrada una vez haya sido utilizado
 * @note Se emplea para el algoritmo de aprendizaje supervisado de propagación hacia atrás (backpropagation)
 *
 * Se computa la sensibilidad de cada una de las neuronas, asumiendo que se trata de una capa oculta, a partir de los
 * sumatorios de pesos y sensibilidad de la capa siguiente (más profunda)
 */
void NeuronLayer::computeHiddenLayerSensitivity(neuron_t* summation) {

  for (int n = 0; n < lSize; ++n)
    layer[n].computeHiddenSensitivity(summation[n]);
  delete [] summation;
}
