/*
 * WeightMatrix.cpp
 *
 *  Created on: 24/11/2014
 *      Author: Cristo
 */

#include "neural_net/WeightMatrix.hpp"

/**
 * @brief Constructor
 * @param inS Tamaño de la capa de neuronas de entrada
 * @param outS Tamaño de la capa de neuronas de salida
 *
 * Constructor en el que se establece el tamaño de la matriz de pesos
 */
WeightMatrix::WeightMatrix(int inS, int outS, bool allocate): inputSize(inS), outputSize(outS), weights(NULL) {

  if (allocate)
    allocateMemory();
}

/**
 * @brief Destructor por defecto
 *
 * Liberación de la memoria reservada para la matriz de pesos
 */
WeightMatrix::~WeightMatrix() {

  freeMemory();
}

/**
 * @brief Obtener los pesos de las sinapsis que llegan a una neurona
 * @param targetNeuron Neurona objetivo de la capa de salida
 * @return Vector con los pesos de las sinapsis que llegan a la neurona objetivo
 * @note La información devuelta es una copia de la original, se puede liberar la memoria sin problema
 */
weight_t* WeightMatrix::getInputWeights(int targetNeuron) {

  weight_t* inWeights = new weight_t[inputSize];

  for (int i = 0; i < inputSize; ++i)
    inWeights[i] = weights[i][targetNeuron];

  return inWeights;
}

/**
 * @brief Obtener los pesos de las sinapsis que salen de una neurona
 * @param sourceNeuron Neurona fuente de la capa de entrada
 * @return Vector con los pesos de las sinapsis que salen de la neurona fuente
 * @note La información devuelta es una copia de la original, se puede liberar la memoria sin problema
 */
weight_t* WeightMatrix::getOutputWeights(int sourceNeuron) {

  weight_t* outWeights = new weight_t[outputSize];

  for (int i = 0; i < outputSize; ++i)
    outWeights[i] = weights[sourceNeuron][i];

  return outWeights;
}

/**
 * @brief Inicializar de forma aleatoria de los pesos de las sinapsis
 * @see Se establece para cada peso un valor aleatorio flotante con DECIMALS decimales de precisión, entre LOWER_WEIGHT y HIGHER_WEIGHT
 */
void WeightMatrix::initWeightsRandom() {

  if (weights == NULL)
    allocateMemory();

  for (int i = 0; i < inputSize; ++i)
    for (int o = 0; o < outputSize; ++o)
      weights[i][o] = randWeightBetween(LOWER_WEIGHT, HIGHER_WEIGHT);
}

/**
 * @brief Inicializar los pesos de las sinapsis a partir de valores dados
 * @param w Matriz que contiene los pesos de las sinapsis
 * @warning La matriz de pesos se asigna sin realizar copia, de modo que no se debe liberar la memoria; de dicha gestión se encarga la propia clase
 */
void WeightMatrix::initWeights(weight_t** w) {

  freeMemory();

  weights = w;
}

void WeightMatrix::mutateWeight(int ni, int no) {

  weights[ni][no] = randWeightBetween(LOWER_WEIGHT, HIGHER_WEIGHT);
}

/**
 * @brief Reservar memoria para los pesos de las sinapsis
 *
 * Se lleva a cabo la reserva de memoria de la matriz de pesos, para aquellos casos en
 * los que no se reciba la matriz del exterior
 */
void WeightMatrix::allocateMemory() {

  weights = new weight_t*[inputSize];
  for (int i = 0; i < inputSize; ++i)
    weights[i] = new weight_t[outputSize];
}

/**
 * @brief Liberar la memoria de los pesos de las sinapsis
 *
 * Se lleva a cabo la liberación de memoria de los pesos de las sinapsis, ya sea para inicializarlos
 * de nuevo, o para la destrucción del objeto
 */
void WeightMatrix::freeMemory() {

  if (weights == NULL)
    return;

  for (int i = 0; i < inputSize; ++i)
    delete [] weights[i];
  delete [] weights;

  weights = NULL;
}
