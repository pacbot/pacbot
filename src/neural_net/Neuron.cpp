/*
 * Neuron.cpp
 *
 *  Created on: 24/11/2014
 *      Author: Cristo
 */

#include "neural_net/Neuron.hpp"

/**
 * @brief Constructor por defecto
 *
 * Constructor en el que se inicializan todos los valores a 0.0
 */
Neuron::Neuron(): inputValue(0.0), outputValue(0.0), sensitivity(0.0) {}

/**
 * @brief Destructor por defecto
 * @note No tiene funcionalidad
 */
Neuron::~Neuron() {}

/**
 * @brief Obtener el valor de entrada de la neurona
 * @return Valor de entrada neto
 */
neuron_t Neuron::getInputValue() { return inputValue; }

/**
 * @brief Obtener el valor de salida de la neurona
 * @return Valor de salida
 */
neuron_t Neuron::getOutputValue() { return outputValue; }

/**
 * @brief Obtener el valor de sensibilidad de la neurona
 * @return Valor de sensibilidad
 * @note Se emplea para el algoritmo de aprendizaje de propagación hacia atrás (backpropagation)
 */
neuron_t Neuron::getSensitivity() { return sensitivity; }

/**
 * @brief Asignar valor de entrada a la neurona
 * @param input Valor de entrada
 *
 * Asignar a la neurona su valor de entrada neto
 */
void Neuron::init(neuron_t input) {

  inputValue = input;
}

/**
 * @brief Computar la salida de la neurona
 * @param first_layer Indica si se trata de una neurona de la primera capa
 * @see Funciones identity y sigmoid
 *
 * Computar la salida de la neurona en función de la entrada actual. Se utiliza la función
 * identidad para neuronas de la capa de salida, y la función sigmoide para el resto de capas
 */
void Neuron::computeOutput(bool first_layer) {

  if (first_layer)
    outputValue = identity(inputValue);
  else
    outputValue = sigmoid(inputValue);
}

/**
 * @brief Computar la sensibilidad de la neurona de salida
 * @param target_value Valor de salida deseado
 * @see Función derivative_sigmoid
 * @note Se emplea para el algoritmo de aprendizaje supervisado de propagación hacia atrás (backpropagation)
 *
 * Se computa la sensibilidad, asumiendo que se trata de una neurona de la capa de salida,
 * a partir de la derivada de la función sigmoide y el error entendido como la diferencia entre
 * el valor de salida de la neurona y el deseado
 */
void Neuron::computeOutputSensitivity(neuron_t target_value) {

  sensitivity =  derivative_sigmoid(inputValue) * (outputValue - target_value);
}

/**
 * @brief Computar la sensibilidad de la neurona oculta
 * @param summation Sumatorio de sensibilidades por los pesos
 * @see Función derivative_sigmoid
 * @note Se emplea para el algoritmo de aprendizaje supervisado de propagación hacia atrás (backpropagation)
 *
 * Se computa la sensibilidad, asumiendo que se trata de una neurona de una capa oculta,
 * a partir de la derivada de la función sigmoide y el sumatorio de los pesos y sensibilidades
 * de la capa siguiente (más profunda)
 */
void Neuron::computeHiddenSensitivity(neuron_t summation) {

  sensitivity = derivative_sigmoid(inputValue) * summation;
}
