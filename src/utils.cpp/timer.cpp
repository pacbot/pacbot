/*
 * timer.cpp
 *
 *  Created on: 29/11/2014
 *      Author: Dani
 */

#include "utils/timer.hpp"

int Timer::s_ids = 0;

Timer::Timer(bool s) {
  m_startTicks = 0;
  m_pausedTicks = 0;

  m_paused = false;
  m_started = false;

  if(s) start();
  m_id = s_ids++;
}

void Timer::start(){
  m_paused = false;
  m_started = true;

  m_startTicks = SDL_GetTicks();
}


void Timer::stop(){
  m_paused = false;
  m_started = false;
}


void Timer::pause(){
  if(m_started && !m_paused){
    m_paused = true;
    m_pausedTicks = SDL_GetTicks() - m_startTicks;
  }
}


void Timer::resume(){
  if(m_paused) {
    m_paused = false;
    m_startTicks = SDL_GetTicks() - m_pausedTicks;

    m_pausedTicks = 0;
  }
}

int Timer::get_ms(){
  if(m_started) {
    if(m_paused) return m_pausedTicks;
    else         return SDL_GetTicks() - m_startTicks;
  }

  return 0;
}

double Timer::get_s(){
  return get_ms()/1000.0;
}


bool Timer::paused(){
  return m_paused;
}


bool Timer::started(){
  return m_started;
}

void Timer::print() {
  std::cout << "[Timer:" << m_id << "]: " << get_ms() << "ms" << std::endl;
}
