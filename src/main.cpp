/*
 * main.cpp
 *
 *  Created on: 25/11/2014
 *      Author: Dani
 */

#include "app/app.hpp"
#include "app/console.hpp"

int main(int argc, char* argv[]) {
  srand(time(NULL));
  ConsoleUI::getInstance().start();
  return 0;
}
