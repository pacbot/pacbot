/*
 * pacman.cpp
 *
 *  Created on: 26/11/2014
 *      Author: Dani
 */

#include "app/config.hpp"
#include "game/pacman.hpp"

Pacman::Pacman(): Object() {
  Config& cfg = Config::getInstance();

  COLOR_DETECT         = cfg.get_color("pacman", "color");
  COLOR_SEARCH_INC     = cfg.get_int("pacman", "search_inc");
  COLOR_SEARCH_WIDTH   = cfg.get_int("pacman", "width");
  COLOR_SEARCH_HEIGHT  = cfg.get_int("pacman", "height");
  COLOR_SEARCH_PERCENT = cfg.get_double("pacman", "search_percent");

  COLOR_SEARCH_OFFSET_X = cfg.get_double("pacman", "search_offset_x");
  COLOR_SEARCH_OFFSET_Y = cfg.get_double("pacman", "search_offset_y");
}

Pacman::~Pacman() {

}
