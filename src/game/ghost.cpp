/*
 * pacman.cpp
 *
 *  Created on: 26/11/2014
 *      Author: Dani
 */

#include "app/config.hpp"
#include "game/ghost.hpp"

std::string to_string(ghost_types_t type) {
  switch(type) {
    case BLINKY: return "blinky";
    case PINKY:  return "pinky";
    case INKY:   return "inky";
    case CLYDE:  return "clyde";
    default:     return "";
  }
}

Ghost::Ghost(ghost_types_t type): Object() {
  std::string section = to_string(type);

  Config& cfg = Config::getInstance();

  m_type = type;

  // General
  VULNERABLE_MAX_DISTANCE         = cfg.get_double("common_default", "vulnerable_max_dist");

  // Normal
  COLOR_DETECT                    = cfg.get_color(section, "color");
  COLOR_SEARCH_INC                = cfg.get_int(section, "search_inc");
  COLOR_SEARCH_WIDTH              = cfg.get_int(section, "width");
  COLOR_SEARCH_HEIGHT             = cfg.get_int(section, "height");
  COLOR_SEARCH_PERCENT            = cfg.get_double(section, "search_percent");
  COLOR_SEARCH_OFFSET_X           = cfg.get_double(section, "search_offset_x");
  COLOR_SEARCH_OFFSET_Y           = cfg.get_double(section, "search_offset_y");;

  // Vulnerabilidad
  VULNERABLE_COLOR_DETECT_1       = cfg.get_color(section, "vulnerable_color_1");
  VULNERABLE_COLOR_DETECT_2       = cfg.get_color(section, "vulnerable_color_2");
  VULNERABLE_OFFSET_X             = cfg.get_double(section, "vulnerable_offset_x");
  VULNERABLE_OFFSET_Y             = cfg.get_double(section, "vulnerable_offset_y");
  VULNERABLE_SEARCH_WIDTH         = cfg.get_int(section, "vulnerable_width");
  VULNERABLE_SEARCH_HEIGHT        = cfg.get_int(section, "vulnerable_height");
  VULNERABLE_COLOR_SEARCH_PERCENT = cfg.get_double(section, "vulnerable_percent");
  VULNERABLE_COLOR_ERROR          = cfg.get_double(section, "vulnerable_error");

  m_vulnerable = false;
}

Ghost::~Ghost() {

}

int Ghost::vulnerable_detect(BYTE *pixels, int width, int height, int x, int y) {
  //mark_detected(false);
  if(detected()) return ALREADY_DETECTED;

  color_t color_actual;
  color_t COLOR_ACTUAL;

  color_actual.b  = (BYTE)pixels[4*(y*width+x)+0];
  color_actual.g = (BYTE)pixels[4*(y*width+x)+1];
  color_actual.r   = (BYTE)pixels[4*(y*width+x)+2];

  int start_x = x + (COLOR_SEARCH_WIDTH * VULNERABLE_OFFSET_X);
  int start_y = y + (COLOR_SEARCH_HEIGHT * VULNERABLE_OFFSET_Y);

  int center_x = start_x + COLOR_SEARCH_WIDTH /2;
  int center_y = start_y + COLOR_SEARCH_HEIGHT/2;

  // Comprobar si estamos en un fantasma vulnerable, y extraer su color.
  if(VULNERABLE_COLOR_DETECT_1.similar(color_actual, VULNERABLE_COLOR_ERROR))
    COLOR_ACTUAL = VULNERABLE_COLOR_DETECT_1;
  else if(VULNERABLE_COLOR_DETECT_2.similar(color_actual, VULNERABLE_COLOR_ERROR)) // <- COCOS!
    COLOR_ACTUAL = VULNERABLE_COLOR_DETECT_2;
  else
    return NOT_DETECTED;

  // Demasiado lejos!!
  if(abs(center_x - getX()) > VULNERABLE_MAX_DISTANCE or abs(center_y - getY()) > VULNERABLE_MAX_DISTANCE)
    return NOT_DETECTED;

  // Buscar en en cuadrado "peque�o"
  for(int i = x; i < x + VULNERABLE_SEARCH_WIDTH; ++i) {
    for(int j = y; j < y + VULNERABLE_SEARCH_HEIGHT; ++j) {
      color_actual.b  = (BYTE)pixels[4*(j*width+i)+0];
      color_actual.g = (BYTE)pixels[4*(j*width+i)+1];
      color_actual.r   = (BYTE)pixels[4*(j*width+i)+2];

      // Si no es del color, salir
      if(!COLOR_ACTUAL.similar(color_actual, VULNERABLE_COLOR_ERROR)) {
        return NOT_DETECTED;
      }
    }
  }

  mark_detected(true);
  mark_vulnerable(true);

  // Centrar el personaje
  setX(center_x);
  setY(center_y);

  return DETECTED;
}

void Ghost::onRender() {
  if(!detected())
    return;

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

  GLint v = COLOR_SEARCH_WIDTH  / 2.0;
  GLint u = COLOR_SEARCH_HEIGHT / 2.0;

  glTranslated(m_x, m_y, 0.0);
  glColor3f( COLOR_DETECT.r/255.f, COLOR_DETECT.g/255.f, COLOR_DETECT.b/255.f );

  glBegin(GL_QUADS);
    glVertex3d(-v,-u, 0);
    glVertex3d( v,-u, 0);
    glVertex3d( v, u, 0);
    glVertex3d(-v, u, 0);
  glEnd();


  if(m_vulnerable) {
    glColor3f( 0.f, 0.f, 0.f );

    GLfloat prev_line_width;
    glGetFloatv(GL_LINE_WIDTH, &prev_line_width);
    glLineWidth(5.0);

    glBegin(GL_LINES);
      // Diagonal1
      glVertex3d(-v, u, 0);
      glVertex3d( v, -u, 0);
      // Diagonal2
      glVertex3d(-v,-u, 0);
      glVertex3d( v, u, 0);
    glEnd();

    glLineWidth(prev_line_width);
  }


  /*glBegin(GL_LINES);
    // Superior
    glVertex3d(-v, u, 0);
    glVertex3d( v, u, 0);
    // Derecha
    glVertex3d( v, u, 0);
    glVertex3d( v,-u, 0);
    // Abajo
    glVertex3d( v,-u, 0);
    glVertex3d(-v,-u, 0);
    // Izquierda
    glVertex3d(-v,-u, 0);
    glVertex3d(-v, u, 0);
    if(m_vulnerable) {
      glColor3f( VULNERABLE_COLOR_DETECT_2.red/255.f, VULNERABLE_COLOR_DETECT_2.green/255.f, VULNERABLE_COLOR_DETECT_2.blue/255.f );

      // Diagonal1
      glVertex3d(-v, u, 0);
      glVertex3d( v, -u, 0);
      // Diagonal2
      glVertex3d(-v,-u, 0);
      glVertex3d( v, u, 0);
    }

    glVertex3d( v, u, 0);
    glVertex3d(-v, u, 0);
  glEnd();*/

  glPopMatrix();
}

bool Ghost::vulnerable() {
  return m_vulnerable;
}

void Ghost::mark_vulnerable(bool value) {
  m_vulnerable = value;
}
