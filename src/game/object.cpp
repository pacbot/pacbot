/*
 * object.cpp
 *
 *  Created on: 25/11/2014
 *      Author: Dani
 */

#include "app/config.hpp"

#include "game/object.hpp"
#include "game/grid.hpp"

Object::Object() {
  m_x = m_y = 0;
  m_detected = false;

  Config& cfg = Config::getInstance();

  COLOR_SEARCH_WIDTH    = cfg.get_int("common_default", "width");
  COLOR_SEARCH_HEIGHT   = cfg.get_int("common_default", "height");
  COLOR_SEARCH_INC      = cfg.get_int("common_default", "search_inc");
  COLOR_ERROR           = cfg.get_double("common_default", "color_error");

  COLOR_SEARCH_PERCENT  = cfg.get_double("common_default", "search_percent");

  COLOR_SEARCH_OFFSET_X = cfg.get_double("common_default", "search_offset_x");
  COLOR_SEARCH_OFFSET_Y = cfg.get_double("common_default", "search_offset_y");
}

Object::~Object() {

}

position_t Object::getX() {
  return m_x;
}

position_t Object::getY() {
  return m_y;
}

void Object::setX(position_t x) {
  m_x = x;
}

void Object::setY(position_t y) {
  m_y = y;
}

bool Object::detected() {
  return m_detected;
}

void Object::mark_detected(bool value) {
  m_detected = value;
}

int Object::detect(BYTE *pixels, int width, int height, int x, int y) {
  //mark_detected(false);
  if(detected()) return ALREADY_DETECTED;

  color_t color_actual; // color_actual actual
  int       pixeles_encontrados = 0;
  const int pixeles_a_encontrar = COLOR_SEARCH_WIDTH * COLOR_SEARCH_HEIGHT * COLOR_SEARCH_PERCENT / (float) COLOR_SEARCH_INC;

  // Si el p�xel actual no es del color deseado, saltar
  color_actual.b  = (BYTE)pixels[4*(y*width+x)+0];
  color_actual.g = (BYTE)pixels[4*(y*width+x)+1];
  color_actual.r   = (BYTE)pixels[4*(y*width+x)+2];

  if(!COLOR_DETECT.similar(color_actual, COLOR_ERROR) )
    return NOT_DETECTED;

  bool success = false;
  int offset_x = COLOR_SEARCH_WIDTH * COLOR_SEARCH_OFFSET_X;
  int offset_y = COLOR_SEARCH_HEIGHT * COLOR_SEARCH_OFFSET_Y;

  int start_x = x + offset_x;
  int start_y = y + offset_y;

  /* Suponiendo que hemos encontrado el p�xel de la "punta" del comecocos (punto superior)
   * nos movemos la mitad de p�xeles del tama�o del cuadrado a analizar hacia la derecha
   * y buscamos */
  for(int i = start_x; !success and i < x + COLOR_SEARCH_WIDTH and i >= 0 and i <= width - COLOR_SEARCH_INC; i += COLOR_SEARCH_INC) {
    for(int j = start_y; !success and  j < y + COLOR_SEARCH_HEIGHT and j >= 0 and j <= height - COLOR_SEARCH_INC; j += COLOR_SEARCH_INC) {
      color_actual.b  = (BYTE)pixels[4*(j*width+i)+0];
      color_actual.g = (BYTE)pixels[4*(j*width+i)+1];
      color_actual.r   = (BYTE)pixels[4*(j*width+i)+2];

      // Comprobar si el p�xel se parece.
      if(COLOR_DETECT.similar(color_actual, COLOR_ERROR) )
        ++pixeles_encontrados;

      // Si ya hemos encontrado todos los p�xeles, dejar de buscar
      if(pixeles_encontrados >= pixeles_a_encontrar) {
        success = true;
      }
    }
  }

  if(success) {
    mark_detected(true);
    // Centrar el personaje
    setX(start_x + COLOR_SEARCH_WIDTH /2);
    setY(start_y + COLOR_SEARCH_HEIGHT/2);

    return DETECTED;
  }
  else {
    return NOT_DETECTED;
  }

}


void Object::onRender() {
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  glTranslated(m_x, m_y, 0.0);
  glColor3f( COLOR_DETECT.r/255.f, COLOR_DETECT.g/255.f, COLOR_DETECT.b/255.f );
  //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

  GLint v = COLOR_SEARCH_WIDTH  / 2.0;
  GLint u = COLOR_SEARCH_HEIGHT / 2.0;

  glBegin(GL_QUADS);
    glVertex3d(-v,-u, 0);
    glVertex3d( v,-u, 0);
    glVertex3d( v, u, 0);
    glVertex3d(-v, u, 0);
  glEnd();

  glPopMatrix();
}

void Object::getGridPos(Grid& g, int& out_x, int& out_y) {
  out_x = getGridX(g);
  out_y = getGridY(g);
}

int Object::getGridX(Grid& g) {
  return g.mapX( getX() );
}

int Object::getGridY(Grid& g) {
  return g.mapY( getY() );
}
