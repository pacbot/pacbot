/*
 * grid.cpp
 *
 *  Created on: 07/12/2014
 *      Author: Dani
 */

#include <cmath>

#include "app/config.hpp"
#include "game/grid.hpp"

Grid::Grid() {
  Config& cfg = Config::getInstance();

  COLUMNS     = cfg.get_int("board", "columns");
  ROWS        = cfg.get_int("board", "rows");
  CELL_WIDTH  = cfg.get_double("board", "cell_width");
  CELL_HEIGHT = cfg.get_double("board", "cell_height");
  START_X     = cfg.get_double("board", "start_x");
  START_Y     = cfg.get_double("board", "start_y");
}

void Grid::onRender() {
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  glColor3f(GRID_COLOR.r/255.f, GRID_COLOR.b/255.f, GRID_COLOR.g/255.f);

  float end_x = get_endX();
  float end_y = get_endY();

  glBegin(GL_LINES);
    // Dibujar l�neas verticales
    for(float x = START_X; x <= end_x; x += CELL_WIDTH) {
      glVertex3f(x, START_Y, 0);
      glVertex3f(x, end_y,   0);
    }

    // Dibujar l�neas horizontales
    for(float y = START_Y; y <= end_y; y += CELL_HEIGHT) {
      glVertex3f(START_X, y, 0);
      glVertex3f(end_x,   y, 0);
    }
  glEnd();

  glPopMatrix();
}

void Grid::map(float in_x, float in_y, int& out_x, int& out_y) {
  out_x = mapX(in_x);
  out_y = mapY(in_y);
}

int Grid::mapX(float in_x) {
  if(in_x < START_X or in_x >= get_endX()) return -1;
  else                                return floor((in_x - START_X) / (CELL_WIDTH) ); // TODO �Funciona?

}

int Grid::mapY(float in_y)  {
  if(in_y < START_Y or in_y >= get_endY()) return -1;
  else                                return floor((in_y - START_Y) / (CELL_HEIGHT) ); // TODO �Funciona?
}
