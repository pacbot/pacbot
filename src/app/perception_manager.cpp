/*
 * perception_manager.cpp
 *
 *  Created on: 12/01/2015
 *      Author: Cristo
 */

#include "app/perception_manager.hpp"

PerceptionManager::PerceptionManager(std::vector<Object*> objects) {

  m_inited = true;

  m_perceptions = NULL;

  m_map = new MapaGrafo();

  m_pacman      = (Pacman*)objects[0];
  m_blinky_red  = (Ghost* )objects[1];
  m_pinky_pink  = (Ghost* )objects[2];
  m_inky_blue   = (Ghost* )objects[3];
  m_clyde_orange = (Ghost* )objects[4];
  m_grid = (Grid*)objects[5];
}

void PerceptionManager::onClose() {
  if (m_perceptions != NULL)
    delete [] m_perceptions;
}

void PerceptionManager::onLoop() {

  neuron_t* input = new neuron_t[N_NEURONS[0]];

  m_map->move(m_pacman->getGridY(*m_grid), m_pacman->getGridX(*m_grid));

  //cout << "Posición: (" << m_pacman->getGridX(*m_grid) << " " << m_pacman->getGridY(*m_grid) << ")" << endl;

  /* Movement */
  input[0] = m_map->movimientoPermitido(arriba);
  input[1] = m_map->movimientoPermitido(abajo);
  input[2] = m_map->movimientoPermitido(derecha);
  input[3] = m_map->movimientoPermitido(izquierda);
  /*cout << "Movement:" << endl;
  cout << input[0] << endl;
  cout << input[1] << endl;
  cout << input[2] << endl;
  cout << input[3] << endl;*/

  /* Ghosts Distance: Red */
  input[4] = m_map->fantasmaCercano(m_blinky_red->getGridY(*m_grid), m_blinky_red->getGridX(*m_grid), arriba);
  input[5] = m_map->fantasmaCercano(m_blinky_red->getGridY(*m_grid), m_blinky_red->getGridX(*m_grid), abajo);
  input[6] = m_map->fantasmaCercano(m_blinky_red->getGridY(*m_grid), m_blinky_red->getGridX(*m_grid), derecha);
  input[7] = m_map->fantasmaCercano(m_blinky_red->getGridY(*m_grid), m_blinky_red->getGridX(*m_grid), izquierda);
  /*cout << "Red:" << endl;
  cout << input[4] << endl;
  cout << input[5] << endl;
  cout << input[6] << endl;
  cout << input[7] << endl;*/

  /* Ghosts Distance: Pink */
  input[8] = m_map->fantasmaCercano(m_pinky_pink->getGridY(*m_grid), m_pinky_pink->getGridX(*m_grid), arriba);
  input[9] = m_map->fantasmaCercano(m_pinky_pink->getGridY(*m_grid), m_pinky_pink->getGridX(*m_grid), abajo);
  input[10] = m_map->fantasmaCercano(m_pinky_pink->getGridY(*m_grid), m_pinky_pink->getGridX(*m_grid), derecha);
  input[11] = m_map->fantasmaCercano(m_pinky_pink->getGridY(*m_grid), m_pinky_pink->getGridX(*m_grid), izquierda);

  /* Ghosts Distance: Cyan */
  input[12] = m_map->fantasmaCercano(m_inky_blue->getGridY(*m_grid), m_inky_blue->getGridX(*m_grid), arriba);
  input[13] = m_map->fantasmaCercano(m_inky_blue->getGridY(*m_grid), m_inky_blue->getGridX(*m_grid), abajo);
  input[14] = m_map->fantasmaCercano(m_inky_blue->getGridY(*m_grid), m_inky_blue->getGridX(*m_grid), derecha);
  input[15] = m_map->fantasmaCercano(m_inky_blue->getGridY(*m_grid), m_inky_blue->getGridX(*m_grid), izquierda);

  /* Ghosts Distance: Orange */
  input[16] = m_map->fantasmaCercano(m_clyde_orange->getGridY(*m_grid), m_clyde_orange->getGridX(*m_grid), arriba);
  input[17] = m_map->fantasmaCercano(m_clyde_orange->getGridY(*m_grid), m_clyde_orange->getGridX(*m_grid), abajo);
  input[18] = m_map->fantasmaCercano(m_clyde_orange->getGridY(*m_grid), m_clyde_orange->getGridX(*m_grid), derecha);
  input[19] = m_map->fantasmaCercano(m_clyde_orange->getGridY(*m_grid), m_clyde_orange->getGridX(*m_grid), izquierda);

  /* Vulnerability */
  input[20] = m_blinky_red->vulnerable();
  input[21] = m_pinky_pink->vulnerable();
  input[22] = m_inky_blue->vulnerable();
  input[23] = m_clyde_orange->vulnerable();

  /* Small Dots Distance */
  input[24] = m_map->cocoCercano(arriba);
  input[25] = m_map->cocoCercano(abajo);
  input[26] = m_map->cocoCercano(derecha);
  input[27] = m_map->cocoCercano(izquierda);

  /* Big Dots Distance */
  input[28] = m_map->cocoGCercano(arriba);
  input[29] = m_map->cocoGCercano(abajo);
  input[30] = m_map->cocoGCercano(derecha);
  input[31] = m_map->cocoGCercano(izquierda);

  if (m_perceptions != NULL)
    delete [] m_perceptions;
  m_perceptions = input;
}
