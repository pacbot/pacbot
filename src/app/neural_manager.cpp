/*
 * neural_manager.cpp
 *
 *  Created on: 11/01/2015
 *      Author: Cristo
 */

#include "app/neural_manager.hpp"

NeuralManager::NeuralManager(PerceptionManager* p_m) {

  m_inited = false;

  m_perception_manager = p_m;
}

bool NeuralManager::onInit(const char* file_input_net) {

  if (file_input_net != NULL) {
    m_red = new MultiLayerPerceptron();
    char* file = new char[SIZE_FILE_NAME];
    strcpy(file, file_input_net);
    m_red->importNet(file);
  }

  m_inited = true;

  return m_inited;
}

void NeuralManager::onClose() {

  if (m_red != NULL)
    delete m_red;
}

void NeuralManager::setNet(MultiLayerPerceptron* red) {

  m_red = red;
}


void NeuralManager::onLoop() {

  if(!m_inited)
    return;

  /* Cálculo de las percepciones y asignación de entrada */
  m_red->setInput(m_perception_manager->getCopyOfPerceptions());

  /* Computar la salida de la red */
  m_red->computeOutput();
}

