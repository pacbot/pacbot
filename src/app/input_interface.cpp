/*
 * input_interface.cpp
 *
 *  Created on: 10/12/2014
 *      Author: Dani
 */

#define _WIN32_WINNT 0x0500
#include <windows.h>
#include <winable.h>

#include "app/config.hpp"
#include "app/input_interface.hpp"

const char* MOVEMENTS_STR[] = {"UP", "DOWN", "RIGHT", "LEFT", "NONE"};

InputInterface::InputInterface(PerceptionManager* p_m) {

  m_perception_manager = p_m;

  m_last_key_pressed = Movements::NONE;
  m_last_move = Movements::NONE;
  m_current_move = Movements::NONE;

  Config& cfg = Config::getInstance();

  m_vk_up    = cfg.get_int("app", "key_up");
  m_vk_left  = cfg.get_int("app", "key_left");
  m_vk_down  = cfg.get_int("app", "key_down");
  m_vk_right = cfg.get_int("app", "key_right");

  m_supervised = false;
  m_iteracion = 0;

  for (int i = 0; i < 4; i++)
    m_available_movements[i] = false;
}

void InputInterface::move(Movements m) {

  bool changed = false, move = true;
  neuron_t* perceptions = m_perception_manager->getPerceptions();
  bool old_movements[4];

  for (int i = 0; i < 4; i++) {
    old_movements[i] = m_available_movements[i];
    if (m_available_movements[i] != perceptions[i])
      changed = true;
    m_available_movements[i] = perceptions[i];
  }

  if (changed) {

    for (int i = 0; i < 4; i++) {
      if (old_movements[i] && !m_available_movements[i] && i != m_current_move)
        move = false;
    }
    if (move || !perceptions[m_current_move]) {
      m_last_move = m_current_move;
      m_current_move = m;
    }
  }

}

bool InputInterface::mustMove() {

  return true;
  //return !m_supervised && m_iteracion % IT_MOVE == 0;
}

void InputInterface::onEvent(SDL_Event* event) {
  if(event->type == SDL_KEYDOWN and event->key.keysym.scancode == SDL_SCANCODE_M) {
    m_supervised = !m_supervised;
  }
}

void InputInterface::onLoop() {
  // Movimiento autom�tico
  if(!m_supervised) {

    if( m_current_move == Movements::NONE)
      return;

    WORD vk;
    m_iteracion++;

    // Presionar tecla actual
    switch(m_current_move) {
      case Movements::UP:
        vk = m_vk_up;
      break;
      case Movements::LEFT:
        vk = m_vk_left;
      break;
      case Movements::DOWN:
        vk = m_vk_down;
      break;
      case Movements::RIGHT:
        vk = m_vk_right;
      break;

      default: return;
    }

    press_key(vk);
  }
  // Supervisado
  else {
    const int DOWN_STATE = 0x0001; //0x8000;

    if( m_last_key_pressed != Movements::UP and (GetAsyncKeyState( m_vk_up ) & DOWN_STATE) ){
      m_last_key_pressed = Movements::UP;
      return;
    }

    if( m_last_key_pressed != Movements::DOWN and (GetAsyncKeyState( m_vk_down ) & DOWN_STATE) ){
      m_last_key_pressed = Movements::DOWN;
      return;
    }

    if( m_last_key_pressed != Movements::LEFT and (GetAsyncKeyState( m_vk_left ) & DOWN_STATE) ){
      m_last_key_pressed = Movements::LEFT;
      return;
    }

    if( m_last_key_pressed != Movements::RIGHT and (GetAsyncKeyState( m_vk_right ) & DOWN_STATE) ){
      m_last_key_pressed = Movements::RIGHT;
      return;
    }
  }

}

void InputInterface::onRender() {
  // TODO: Dibujar direcci�n de la flecha en una esquina
  // ...
}


void InputInterface::click_relative(ScreenAnalizer* s, int x, int y) {
  rect_t srect = s->getBounds();
  click(srect.x + x, srect.y + y);
}

void InputInterface::click(int x, int y) {
  // Constantes (métricas de pantalla).
  const double XSCALEFACTOR = 65535 / (GetSystemMetrics(SM_CXSCREEN) - 1);
  const double YSCALEFACTOR = 65535 / (GetSystemMetrics(SM_CYSCREEN) - 1);

  // Guardar posición actual para restablecer
  POINT save_pos;
  GetCursorPos(&save_pos);

  // Pulsar en pantalla
  INPUT input={0};
  input.type = INPUT_MOUSE;
  input.mi.dx = (LONG)x * XSCALEFACTOR;
  input.mi.dy = (LONG)y * YSCALEFACTOR;
  input.mi.dwFlags = MOUSEEVENTF_MOVE | MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP;

  SendInput(1,&input,sizeof(INPUT));

  // Volver a colocar el ratón en su sitio.  input.type = INPUT_MOUSE;
  input.mi.dx = (LONG)save_pos.x * XSCALEFACTOR;
  input.mi.dy = (LONG)save_pos.y * YSCALEFACTOR;
  input.mi.dwFlags = MOUSEEVENTF_MOVE | MOUSEEVENTF_ABSOLUTE;

  //SendInput(1,&input,sizeof(INPUT));
}

void InputInterface::press_key(WORD vk) {
  INPUT ip;

  // Set up a generic keyboard event.
  ip.type = INPUT_KEYBOARD;
  ip.ki.wScan = 0; // hardware scan code for key
  ip.ki.time = 0;
  ip.ki.dwExtraInfo = 0;
  ip.ki.wVk = vk;

  // Presionar
  ip.ki.dwFlags = 0x00;
  SendInput(1, &ip, sizeof(INPUT));

  // Soltar
  ip.ki.dwFlags = KEYEVENTF_KEYUP;
  SendInput(1, &ip, sizeof(INPUT));
}
