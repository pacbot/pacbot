/*
 * debug_view.cpp
 *
 *  Created on: 29/11/2014
 *      Author: Dani
 */

#include <sstream>

#include "app/app.hpp"
#include "app/view.hpp"
#include "game/grid.hpp"

//std::vector<Object&> m_objects; /**< Lista de objetos a representar */
//SDL_Window*   m_window;     /**< Ventana SDL */
//SDL_GLContext m_GLcontext;  /**< Contexto openGL */

GameView::GameView(int width, int height, std::vector<Object*> objects) {
  m_GLcontext = 0;
  m_window    = NULL;

  m_objects = objects;
  m_width = width;
  m_height = height;

  m_default_font = NULL;

  m_grid = (Grid*)m_objects[m_objects.size() - 1];
}

bool GameView::onInit() {
  // Cargar fuente por defecto
  m_default_font = TTF_OpenFont(DEFAULT_FONT_FILE, DEFAULT_FONT_SIZE);
  if(!m_default_font) {
    std::cout << "Unable to load default ttf font: " << TTF_GetError() << std::endl;
    return false;
  }

  // Iniciar OpenGL
  SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 0);
  SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 0);

  m_window = SDL_CreateWindow( App::TITLE, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, m_width + DEBUG_WIDTH, m_height + DEBUG_HEIGHT, SDL_WINDOW_OPENGL);
  if(!m_window) {
    std::cerr << "Error creando ventana SDL2: " << SDL_GetError() << std::endl;
    return false;
  }

  // Iniciar openGL
  m_GLcontext = SDL_GL_CreateContext(m_window);
  SDL_GL_MakeCurrent(m_window, m_GLcontext);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  glClearColor(0, 0, 0, 1);
  glClearDepth(1.0f);

  glEnable(GL_TEXTURE_2D);
  glShadeModel(GL_SMOOTH);

  glEnable( GL_BLEND );
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LEQUAL);

  glViewport(0, DEBUG_HEIGHT, m_width, m_height);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0.0, m_width, m_height, 0.0, 0.0, 1.0);

  /*glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
  glHint(GL_POINT_SMOOTH_HINT,GL_NICEST);
  glHint(GL_LINE_SMOOTH_HINT, GL_NICEST );
  glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST );

  //glEnable(GL_PERSPECTIVE_CORRECTION);
  glEnable(GL_POINT_SMOOTH);
  glEnable(GL_LINE_SMOOTH);
  glEnable(GL_POLYGON_SMOOTH);*/

  glEnable(GL_COLOR_MATERIAL);

  return true;
}

void GameView::onClose() {
  TTF_CloseFont(m_default_font);

  SDL_GL_DeleteContext(m_GLcontext);
  SDL_DestroyWindow(m_window);
}

void GameView::onRender() {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glViewport(0, DEBUG_HEIGHT, m_width, m_height);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0.0, m_width, m_height, 0.0, 0.0, 1.0);
  for(std::vector<Object*>::iterator it = m_objects.begin(); it != m_objects.end(); ++it) {
    (*it)->onRender();
  }

  onDebugRender();

  SDL_GL_SwapWindow(m_window);
  //SDL_RenderPresent(m_sdl_renderer);
  //SDL_GL_SwapWindow(m_window);
}

void GameView::drawText(const char* text, TTF_Font* font, SDL_Color color, int x, int y) {
  Uint32 rmask, gmask, bmask, amask;

  /* SDL interprets each pixel as a 32-bit number, so our masks must depend
     on the endianness (byte order) of the machine */
  #if SDL_BYTEORDER == SDL_BIG_ENDIAN
      rmask = 0xff000000;
      gmask = 0x00ff0000;
      bmask = 0x0000ff00;
      amask = 0x000000ff;
  #else
      rmask = 0x000000ff;
      gmask = 0x0000ff00;
      bmask = 0x00ff0000;
      amask = 0xff000000;
  #endif


  // TEXTO DEL PACMAN!
  SDL_Rect area;
  SDL_Surface *sText = TTF_RenderUTF8_Blended( font, text, color );
  area.x = 0;area.y = 0;area.w = sText->w;area.h = sText->h;
  SDL_Surface* temp = SDL_CreateRGBSurface(0, sText->w,sText->h,32, rmask, gmask, bmask, amask);
  if(temp == NULL) {
    std::cout << SDL_GetError() << std::endl;
    return;
  }

  SDL_BlitSurface(sText, &area, temp, NULL);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, sText->w, sText->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, temp->pixels);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glTranslatef(x, y, 0.f);
  glColor3f(1.0, 1.0, 1.0);

  glEnable(GL_TEXTURE_2D);
  glBegin(GL_QUADS); {
      glTexCoord2d(0, 0); glVertex3f(0, 0, 0);
      glTexCoord2d(1, 0); glVertex3f(0 + sText->w, 0, 0);
      glTexCoord2d(1, 1); glVertex3f(0 + sText->w, 0 + sText->h, 0);
      glTexCoord2d(0, 1); glVertex3f(0, 0 + sText->h, 0);
  } glEnd();
  glDisable(GL_TEXTURE_2D);
  SDL_FreeSurface( sText );
  SDL_FreeSurface( temp );
}

void GameView::onDebugRender() {
  drawText(App::TITLE, m_default_font, DEBUG_PACMAN_COLOR, DEBUG_PACMAN_X, DEBUG_PACMAN_Y);

  // Pintar texto de depuraci�n
  glViewport(0, 0, m_width, DEBUG_HEIGHT);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0.0, m_width, DEBUG_HEIGHT, 0.0, 0.0, 1.0);

  // Pintar posici�n del jugador
  std::stringstream ss;
  drawText("Pacman:", m_default_font, DEBUG_PACMAN_COLOR, DEBUG_PACMAN_X, DEBUG_PACMAN_Y);
  ss << m_objects[0]->getGridX(*m_grid) << ", " << m_objects[0]->getGridY(*m_grid);
  drawText(ss.str().c_str(), m_default_font, DEBUG_PACMAN_COLOR, DEBUG_PACMAN_X + DEBUG_TEXT_OFFSET, DEBUG_PACMAN_Y);

  // Fantasmas
  drawText("Blinky:", m_default_font, DEBUG_BLINKY_COLOR, DEBUG_BLINKY_X, DEBUG_BLINKY_Y);
  ss.str(""); ss.clear(); ss << m_objects[1]->getGridX(*m_grid) << ", " << m_objects[1]->getGridY(*m_grid);
  drawText(ss.str().c_str(), m_default_font, DEBUG_BLINKY_COLOR, DEBUG_BLINKY_X + DEBUG_TEXT_OFFSET, DEBUG_BLINKY_Y);

  drawText("Pinky: ", m_default_font, DEBUG_PINKY_COLOR, DEBUG_PINKY_X, DEBUG_PINKY_Y);
  ss.str(""); ss.clear(); ss << m_objects[2]->getGridX(*m_grid) << ", " << m_objects[2]->getGridY(*m_grid);
  drawText(ss.str().c_str(), m_default_font, DEBUG_PINKY_COLOR, DEBUG_PINKY_X + DEBUG_TEXT_OFFSET, DEBUG_PINKY_Y);

  drawText("Inky:", m_default_font, DEBUG_INKY_COLOR, DEBUG_INKY_X, DEBUG_INKY_Y);
  ss.str(""); ss.clear(); ss << m_objects[3]->getGridX(*m_grid) << ", " << m_objects[3]->getGridY(*m_grid);
  drawText(ss.str().c_str(), m_default_font, DEBUG_INKY_COLOR, DEBUG_INKY_X + DEBUG_TEXT_OFFSET, DEBUG_INKY_Y);

  drawText("Clyde:", m_default_font, DEBUG_CLYDE_COLOR, DEBUG_CLYDE_X, DEBUG_CLYDE_Y);
  ss.str(""); ss.clear(); ss << m_objects[4]->getGridX(*m_grid) << ", " << m_objects[4]->getGridY(*m_grid);
  drawText(ss.str().c_str(), m_default_font, DEBUG_CLYDE_COLOR, DEBUG_CLYDE_X + DEBUG_TEXT_OFFSET, DEBUG_CLYDE_Y);

  // Texto de pausa
  if(App::getInstance().isPaused())
    drawText(DEBUG_PAUSE, m_default_font, DEBUG_PAUSE_COLOR, DEBUG_PAUSE_X, DEBUG_PAUSE_Y);
}
