/*
 * screen_analizer.cpp
 *
 *  Created on: 25/11/2014
 *      Author: Dani
 */

#include <sstream>

#include "app/screen_analizer.hpp"
#include "app/app.hpp"
#include "app/config.hpp"

ScreenAnalizer::ScreenAnalizer(std::vector<Object*> objects) {
  m_h_screenshot = NULL;
  m_bitmap_info  = NULL;

  Config& cfg = Config::getInstance();

  m_bounds.x      = cfg.get_int("screen_analizer", "x");
  m_bounds.y      = cfg.get_int("screen_analizer", "y");
  m_bounds.width  = cfg.get_int("screen_analizer", "width");
  m_bounds.height = cfg.get_int("screen_analizer", "height");
  m_inited = true;

  MIN_X = cfg.get_int("screen_analizer", "min_x");
  MIN_Y = cfg.get_int("screen_analizer", "min_y");
  MAX_X = cfg.get_int("screen_analizer", "max_x");
  MAX_Y = cfg.get_int("screen_analizer", "max_y");
  INC_X = cfg.get_int("screen_analizer", "inc_x");
  INC_Y = cfg.get_int("screen_analizer", "inc_y");

  // Comecocos
  m_pacman      = (Pacman*)objects[0];
  m_blinky_red  = (Ghost* )objects[1];
  m_pinky_pink  = (Ghost* )objects[2];
  m_inky_blue   = (Ghost* )objects[3];
  m_clyde_orange = (Ghost* )objects[4];

  m_draw = false;
  m_game_over = false;

  // Fin del juego
  GAME_OVER_X              = cfg.get_int("game_over", "x");
  GAME_OVER_Y              = cfg.get_int("game_over", "y");
  GAME_OVER_WIDTH          = cfg.get_int("game_over", "width");
  GAME_OVER_HEIGHT         = cfg.get_int("game_over", "height");
  GAME_OVER_COLOR          = cfg.get_color("game_over", "color");
  GAME_OVER_COLOR_ERROR    = cfg.get_double("game_over", "color_error");
  GAME_OVER_SEARCH_PERCENT = cfg.get_double("game_over", "search_percent");
}


// http://www.codeproject.com/Articles/5051/Various-methods-for-capturing-the-screen
void ScreenAnalizer::onLoop() {
  if(!m_inited)
    return;

  // Contador de frames para guardar las im�genes.
  static int frame = -1;
  ++frame;

  // Capturar imagen
  HWND hDesktopWnd         = GetDesktopWindow();
  HDC hDesktopDC           = GetDC(hDesktopWnd);
  HDC hCaptureDC           = CreateCompatibleDC(hDesktopDC);
  m_h_screenshot           = CreateCompatibleBitmap(hDesktopDC, m_bounds.width, m_bounds.height);

  SelectObject(hCaptureDC, m_h_screenshot);
  BitBlt(hCaptureDC, 0, 0, m_bounds.width, m_bounds.height, hDesktopDC, m_bounds.x, m_bounds.y, SRCCOPY|CAPTUREBLT);

  // Obtener mapa de bits para procesar
  m_bitmap_info = createBitmapInfo(); // XXX: Posible fuga de memoria
  GetObject(m_h_screenshot, sizeof(BITMAP), (LPSTR)&m_bitmap);

  // Procesar imagen (a partir de m_bitmap.bmBits)
  process_screenshot();

  // Depuraci�n: guardar imagen cada X fotogramas
  /*int X = 10;
  if(frame % X == 0) {
    std::stringstream ss;
    ss << "assets/debug/screenshot_" << frame << ".bmp";
    //std::cout << ss.str() << std::endl;
    if(!screenshotToFile(hCaptureDC, ss.str()))
      std::cout << "Error code: " << GetLastError() <<  std::endl;
  }*/

  // Limpiar estructuras de datos
  ReleaseDC(hDesktopWnd, hDesktopDC);
  DeleteDC(hCaptureDC);
  DeleteObject(m_h_screenshot); // �?
}

void ScreenAnalizer::onEvent(SDL_Event* event) {
  if(event->type == SDL_KEYDOWN && event->key.keysym.scancode == SDL_SCANCODE_D) {
    m_draw = !m_draw;
  }
}

void ScreenAnalizer::onRender() {
  if(!m_draw) return;

  // Dibujar rect�ngulo del �rea a analizar
  HDC hDC_Desktop = GetDC(0);

  HBRUSH brush=CreateSolidBrush( RGB(31,249,0) );
  SelectObject( hDC_Desktop, brush ); // �?

  // Dibujar rect�ngulos para cada borde
  RECT arriba    = {m_bounds.x, m_bounds.y, m_bounds.x + m_bounds.width + BORDER_WIDTH * 2, m_bounds.y + BORDER_WIDTH};
  RECT derecha   = {m_bounds.x + m_bounds.width + BORDER_WIDTH, m_bounds.y, m_bounds.x + m_bounds.width + BORDER_WIDTH*2, m_bounds.y + m_bounds.height + BORDER_WIDTH*2};
  RECT abajo     = {m_bounds.x, m_bounds.y + m_bounds.height + BORDER_WIDTH, m_bounds.x + m_bounds.width + BORDER_WIDTH * 2, m_bounds.y + m_bounds.height + BORDER_WIDTH*2};
  RECT izquierda = {m_bounds.x, m_bounds.y, m_bounds.x + BORDER_WIDTH, m_bounds.y + m_bounds.height + BORDER_WIDTH*2};

  /* FIXME: Lento */
  FillRect(hDC_Desktop, &arriba, brush);
  FillRect(hDC_Desktop, &derecha, brush);
  FillRect(hDC_Desktop, &abajo, brush);
  FillRect(hDC_Desktop, &izquierda, brush);

  DeleteObject(brush);
}

void ScreenAnalizer::reset_objets() {
  m_pacman->mark_detected(false);

  m_blinky_red->mark_detected(false);
  m_pinky_pink->mark_detected(false);
  m_inky_blue->mark_detected(false);
  m_clyde_orange->mark_detected(false);

  m_blinky_red->mark_vulnerable(false);
  m_pinky_pink->mark_vulnerable(false);
  m_inky_blue->mark_vulnerable(false);
  m_clyde_orange->mark_vulnerable(false);

  /*m_pacman->setX(-1);       m_pacman->setY(-1);
  m_blinky_red->setX(-1);   m_blinky_red->setY(-1);
  m_pinky_pink->setX(-1);   m_pinky_pink->setY(-1);
  m_inky_blue->setX(-1);    m_inky_blue->setY(-1);
  m_clyde_orange->setX(-1); m_clyde_orange->setY(-1);*/
}

void ScreenAnalizer::process_screenshot() {
  int width  = m_bitmap.bmWidth;
  int height = m_bitmap.bmHeight;

  LONG size = width * height * 4;
  BYTE bytes [size];

  GetBitmapBits (m_h_screenshot, size, bytes);

  const int NUM_OBJECTS = 5;
  int remaining = NUM_OBJECTS;

  reset_objets();

  if(App::getInstance().getAppOption() != RUN_BOT && App::getInstance().getAppOption() != TRAIN_SUPERVISED && game_over(bytes, width, height)) {
    // ...
    return;
  }

  // Para cada p�xel, buscar elementos
  // TODO: A�adir bordes a eliminar.
  // Primer barrido: detectar todos los elementos
  for (int y = MIN_Y; y <= MAX_Y - INC_Y && remaining > 0; y += INC_Y){
    for(int x = MIN_X; x <= MAX_X - INC_X && remaining > 0; x += INC_X){

      if(m_pacman->detect(bytes, width, height, x, y) > 0) {
        //std::cout << "Pacman found in " << m_pacman->getX() << "," << m_pacman->getY() << std::endl;
        remaining--;
      }
      else if(m_blinky_red->detect(bytes, width, height, x, y) > 0) {
        //std::cout << "Red found in " << m_blinky_red->getX() << "," << m_blinky_red->getY() << std::endl;
        remaining--;
      }
      else if(m_pinky_pink->detect(bytes, width, height, x, y) > 0) {
        //std::cout << "Pink found in " << m_pinky_pink->getX() << "," << m_pinky_pink->getY() << std::endl;
        remaining--;
      }
      else if(m_inky_blue->detect(bytes, width, height, x, y) > 0) {
        //std::cout << "Blue found in " << m_inky_blue->getX() << "," << m_inky_blue->getY() << std::endl;
        remaining--;
      }
      else if(m_clyde_orange->detect(bytes, width, height, x, y) > 0) {
        //std::cout << "Orange found in " << m_clyde_orange->getX() << "," << m_clyde_orange->getY() << std::endl;
        remaining--;
      }
    }
  }

  // Segundo barrido: detectar solo fantasmas vulnerables
  for (int y = 0; y <=  height - INC_Y && remaining > 0; y += INC_Y){
    for(int x = 0; x <=  width - INC_X && remaining > 0; x += INC_X){
      if(m_blinky_red->vulnerable_detect(bytes, width, height, x, y) > 0) {
        //std::cout << "Red(X) found in " << m_blinky_red->getX() << "," << m_blinky_red->getY() << std::endl;
        remaining--;
      }
      else if(m_pinky_pink->vulnerable_detect(bytes, width, height, x, y) > 0) {
        //std::cout << "Pink(X) found in " << m_pinky_pink->getX() << "," << m_pinky_pink->getY() << std::endl;
        remaining--;
      }
      else if(m_inky_blue->vulnerable_detect(bytes, width, height, x, y) > 0) {
        //std::cout << "Blue(X) found in " << m_inky_blue->getX() << "," << m_inky_blue->getY() << std::endl;
        remaining--;
      }
      else if(m_clyde_orange->vulnerable_detect(bytes, width, height, x, y) > 0) {
        //std::cout << "Orange(X) found in " << m_clyde_orange->getX() << "," << m_clyde_orange->getY() << std::endl;
        remaining--;
      }
    }
  }
  /*m_blinky_red->vulnerable_detect(bytes, width, height);
  m_pinky_pink->vulnerable_detect(bytes, width, height);
  m_inky_blue->vulnerable_detect(bytes, width, height);
  m_clyde_orange->vulnerable_detect(bytes, width, height);*/

}

bool ScreenAnalizer::game_over(BYTE *pixels, int width, int height) {
  if(m_game_over)
    return m_game_over;

  color_t color_actual; // color_actual actual
  int       pixeles_encontrados = 0;
  const int pixeles_a_encontrar = GAME_OVER_WIDTH * GAME_OVER_HEIGHT * GAME_OVER_SEARCH_PERCENT;

  bool success = false;

  /* Suponiendo que hemos encontrado el p�xel de la "punta" del comecocos (punto superior)
   * nos movemos la mitad de p�xeles del tama�o del cuadrado a analizar hacia la derecha
   * y buscamos */
  for(int i = GAME_OVER_X; !success and i < GAME_OVER_X + GAME_OVER_WIDTH; ++i) {
    for(int j = GAME_OVER_Y; !success and  j < GAME_OVER_Y + GAME_OVER_HEIGHT; ++j) {
      color_actual.b  = (BYTE)pixels[4*(j*width+i)+0];
      color_actual.g = (BYTE)pixels[4*(j*width+i)+1];
      color_actual.r   = (BYTE)pixels[4*(j*width+i)+2];

      // Comprobar si el p�xel se parece.
      if(GAME_OVER_COLOR.similar(color_actual, GAME_OVER_COLOR_ERROR) )
        ++pixeles_encontrados;

      // Si ya hemos encontrado todos los p�xeles, dejar de buscar
      if(pixeles_encontrados >= pixeles_a_encontrar) {
        success = true;
      }
    }
  }

  if(!success) {
    m_game_over = true;
    return m_game_over;
  }

  return m_game_over;
}

PBITMAPINFO ScreenAnalizer::createBitmapInfo() {
  BITMAP bmp;
  PBITMAPINFO pbmi;
  WORD    cClrBits;

  // Retrieve the bitmap color format, width, and height.
  if (!GetObject(m_h_screenshot, sizeof(BITMAP), (LPSTR)&bmp))
    return NULL;

  // Convert the color format to a count of bits.
  cClrBits = (WORD)(bmp.bmPlanes * bmp.bmBitsPixel);
  if (cClrBits == 1)       cClrBits = 1;
  else if (cClrBits <= 4)  cClrBits = 4;
  else if (cClrBits <= 8)  cClrBits = 8;
  else if (cClrBits <= 16) cClrBits = 16;
  else if (cClrBits <= 24) cClrBits = 24;
  else                     cClrBits = 32;

  // Allocate memory for the BITMAPINFO structure. (This structure
  // contains a BITMAPINFOHEADER structure and an array of RGBQUAD
  // data structures.)
   if (cClrBits < 24) pbmi = (PBITMAPINFO) LocalAlloc(LPTR, sizeof(BITMAPINFOHEADER) + sizeof(RGBQUAD) * (1<< cClrBits));
   // There is no RGBQUAD array for these formats: 24-bit-per-pixel or 32-bit-per-pixel
   else               pbmi = (PBITMAPINFO) LocalAlloc(LPTR, sizeof(BITMAPINFOHEADER));

  // Initialize the fields in the BITMAPINFO structure.
  pbmi->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
  pbmi->bmiHeader.biWidth = bmp.bmWidth;
  pbmi->bmiHeader.biHeight = bmp.bmHeight;
  pbmi->bmiHeader.biPlanes = bmp.bmPlanes;
  pbmi->bmiHeader.biBitCount = bmp.bmBitsPixel;
  if (cClrBits < 24)
    pbmi->bmiHeader.biClrUsed = (1<<cClrBits);

  // If the bitmap is not compressed, set the BI_RGB flag.
  pbmi->bmiHeader.biCompression = BI_RGB;

  // Compute the number of bytes in the array of color
  // indices and store the result in biSizeImage.
  // The width must be DWORD aligned unless the bitmap is RLE
  // compressed.
  pbmi->bmiHeader.biSizeImage = ((pbmi->bmiHeader.biWidth * cClrBits +31) & ~31) /8 * pbmi->bmiHeader.biHeight;
  // Set biClrImportant to 0, indicating that all of the
  // device colors are important.
  pbmi->bmiHeader.biClrImportant = 0;
  return pbmi;
}

bool ScreenAnalizer::screenshotToFile(HDC hDC, std::string file) {
  HANDLE hf;                 // file handle
  BITMAPFILEHEADER hdr;       // bitmap file-header
  PBITMAPINFOHEADER pbih;     // bitmap info-header
  LPBYTE lpBits;              // memory pointer
  //DWORD dwTotal;              // total count of bytes
  DWORD cb;                   // incremental count of bytes
  BYTE *hp;                   // byte pointer
  DWORD dwTmp;

  pbih = (PBITMAPINFOHEADER) m_bitmap_info;
  lpBits = (LPBYTE) GlobalAlloc(GMEM_FIXED, pbih->biSizeImage);

  if (!lpBits)
    return false;

  // Retrieve the color table (RGBQUAD array) and the bits
  // (array of palette indices) from the DIB.
  if (!GetDIBits(hDC, m_h_screenshot, 0, (WORD) pbih->biHeight, lpBits, m_bitmap_info, DIB_RGB_COLORS))
    return false;

  // Create the .BMP file.
  hf = CreateFile(file.c_str(), GENERIC_READ | GENERIC_WRITE,(DWORD) 0, NULL,CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL, (HANDLE) NULL);
  if (hf == INVALID_HANDLE_VALUE)
    return false;

  hdr.bfType = 0x4d42;        // 0x42 = "B" 0x4d = "M"
  // Compute the size of the entire file.
  hdr.bfSize = (DWORD) (sizeof(BITMAPFILEHEADER) + pbih->biSize + pbih->biClrUsed * sizeof(RGBQUAD) + pbih->biSizeImage);
  hdr.bfReserved1 = 0;
  hdr.bfReserved2 = 0;

  // Compute the offset to the array of color indices.
  hdr.bfOffBits = (DWORD) sizeof(BITMAPFILEHEADER) + pbih->biSize + pbih->biClrUsed * sizeof (RGBQUAD);

  // Copy the BITMAPFILEHEADER into the .BMP file.
  if (!WriteFile(hf, (LPVOID) &hdr, sizeof(BITMAPFILEHEADER), (LPDWORD) &dwTmp,  NULL))
    return false;

  // Copy the BITMAPINFOHEADER and RGBQUAD array into the file.
  if (!WriteFile(hf, (LPVOID) pbih, sizeof(BITMAPINFOHEADER) + pbih->biClrUsed * sizeof (RGBQUAD),(LPDWORD) &dwTmp, ( NULL)))
    return false;

  // Copy the array of color indices into the .BMP file.
  /*dwTotal = */cb = pbih->biSizeImage;
  hp = lpBits;
  if (!WriteFile(hf, (LPSTR) hp, (int) cb, (LPDWORD) &dwTmp,NULL))
    return false;

  // Close the .BMP file.
   if (!CloseHandle(hf))
     return false;

  // Free memory.
  GlobalFree((HGLOBAL)lpBits);

  return true;
}

rect_t ScreenAnalizer::getBounds() {
  return m_bounds;
}

