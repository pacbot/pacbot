/*
 * console.cpp
 *
 *  Created on: 16/01/2015
 *      Author: Dani
 */

#include <climits>
#include <limits>

#include "global.hpp"
#include "app/app.hpp"
#include "app/console.hpp"
#include "neural_net/MultiLayerPerceptron.hpp"

using namespace std;

int cin_int(int min, int max, string prompt = "> ") {
  int value = INT_MIN;

  while(value < min or value > max) {
    cout << prompt;
    cin >> value;

    if(!cin.good()) {
      value = INT_MIN;
      cin.clear();
      cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    }
  }

  cin.clear();
  cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');

  return value;
}

ConsoleUI::ConsoleUI() {

}

void ConsoleUI::start() {
  const int NUM_OPCIONES = 5;
  int select = 0;

  // Ejecutar continuamente.
  while(select != NUM_OPCIONES) {
    cout << endl << "====== Pac-Bot ======" << endl;
    cout << "Seleccione una opción: " << endl;
    cout << "1. Ejecutar bot." << endl;
    cout << "2. Entrenamiento con algoritmo genético. " << endl;
    cout << "3. Entrenamiento supervisado. " << endl;
    cout << "4. Entrenar red." << endl;
    cout << "5. Salir." << endl;

    select = cin_int(1, NUM_OPCIONES);

    switch(select) {
      case 1: run_bot();          break;
      case 2: train_genetic();    break;
      case 3: train_supervised(); break;
      case 4: train_net();        break;
      case 5: return;
    }
  }
}

void ConsoleUI::run_bot() {
  cout << endl << "  - Ejecutar bot" << endl;
  // Pedir nombre de fichero
  string fichero;
  cout << endl << "Fichero de la red neuronal (por defecto es \"" << DEFAULT_NET_FILE << "\"): " << endl;
  cout << "> ";
  getline(cin, fichero, '\n');
  if(fichero == "") fichero = DEFAULT_NET_FILE;

  // Ejecutar bot
  vector<string> params = {fichero};
  App::getInstance().onExecute(RUN_BOT, params);
}

void ConsoleUI::train_genetic() {
  cout << endl << "  - Entrenamiento genético." << endl;
  int poblacion, generaciones, n_seleccionados, mutaciones_min, mutaciones_max, juegos_por_bot;

  cout << "Población (mayor que 0): " << endl;
  poblacion = cin_int(1, INT_MAX);

  cout << "Generaciones (mayor que 0): " << endl;
  generaciones = cin_int(1, INT_MAX);

  cout << "Número de mejores redes seleccionadas (en cada generación, mayor que 1, menor o igual que " << poblacion << "): " << endl;
  n_seleccionados = cin_int(2, poblacion);

  cout << "Número mínimo de mutaciones (en cada generación, mayor o igual que 0): " << endl;
  mutaciones_min = cin_int(0, INT_MAX);

  cout << "Número máximo de mutaciones (en cada generación, mayor o igual que " << mutaciones_min << "): " << endl;
  mutaciones_max = cin_int(mutaciones_min, INT_MAX);

  cout << "Número de partidas jugadas por bot (mayor que 0)" << endl;
  juegos_por_bot = cin_int(1, INT_MAX);


  // Entrenamiento genético
  vector<string> params = {SSTR(poblacion), SSTR(generaciones), SSTR(n_seleccionados), SSTR(mutaciones_min), SSTR(mutaciones_max), SSTR(juegos_por_bot)};
  App::getInstance().onExecute(TRAIN_GENETIC, params);
}

void ConsoleUI::train_supervised() {
  cout << endl << "  - Entrenamiento supervisado" << endl;
  // Pedir nombre de fichero
  string fichero_dest;
  cout << endl << "Fichero destino para guardar el entrenamiento (por defecto es \"" << DEFAULT_TRAIN_FILE << "\"): " << endl;
  cout << "> ";
  getline(cin, fichero_dest, '\n');
  if(fichero_dest == "") fichero_dest = DEFAULT_TRAIN_FILE;

  // Entrenamiento supervisado
  vector<string> arguments = { fichero_dest };
  App::getInstance().onExecute(TRAIN_SUPERVISED, arguments);
}

void ConsoleUI::train_net() {
  cout << endl << "  - Entrenamiento de red" << endl;
  // Pedir nombres de ficheros
  string fichero_red_a_entrenar;
  cout << endl << "Fichero de la red a entrenar (por defecto se generará una red aleatoria):" << endl;
  cout << "> ";
  getline(cin, fichero_red_a_entrenar, '\n');

  string fichero_entrenamiento;
  cout << endl << "Fichero de entrenamiento (por defecto es \"" << DEFAULT_TRAIN_FILE << "\"): " << endl;
  cout << "> ";
  getline(cin, fichero_entrenamiento, '\n');
  if(fichero_entrenamiento == "") fichero_entrenamiento = DEFAULT_TRAIN_FILE;

  string fichero_red_resultante = "";
  cout << endl << "Fichero de la red entrenada: " << endl;
  cout << "> ";
  while(fichero_red_resultante == "") {
    getline(cin, fichero_red_resultante, '\n');
  }

  // Entrenar red.

  MultiLayerPerceptron red;

  char* training_file = new char [SIZE_FILE_NAME];
  strcpy(training_file, fichero_entrenamiento.c_str());

  if (fichero_red_a_entrenar == "") {
    red.completeTraining(training_file);
  }
  else {
    char* input_file = new char [SIZE_FILE_NAME];
    strcpy(input_file, fichero_red_a_entrenar.c_str());
    red.completeTraining(training_file, input_file);
  }

  char* output_file = new char[SIZE_FILE_NAME];
  strcpy(output_file, fichero_red_resultante.c_str());
  red.exportNet(output_file);

}


