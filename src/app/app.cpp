/*
 * app.cpp
 *
 *  Created on: 25/11/2014
 *      Author: Dani
 */

#include "app/app.hpp"
#include "app/config.hpp"

#include "utils/timer.hpp"

App::App(): m_running(false), m_paused(true), m_blinky_red(BLINKY), m_pinky_pink(PINKY), m_inky_blue(INKY), m_clyde_orange(CLYDE) {
  Config& cfg = Config::getInstance();

  START_GAME_DELAY   = cfg.get_int("app", "start_game_delay");
  START_DELAY        = cfg.get_int("app", "start_delay");
  RESET_BUTTON_REL_X = cfg.get_int("app", "reset_button_rel_x");
  RESET_BUTTON_REL_Y = cfg.get_int("app", "reset_button_rel_y");
  RESET_KEY          = cfg.get_int("app", "reset_key");
  PLAY_BUTTON_REL_X  = cfg.get_int("app", "play_button_rel_x");
  PLAY_BUTTON_REL_Y  = cfg.get_int("app", "play_button_rel_y");
}

App::~App() {

}

bool App::onInit() {
  // Iniciar objetos y todo eso
  std::vector<Object*> objetos = {&m_pacman, &m_blinky_red, &m_pinky_pink, &m_inky_blue, &m_clyde_orange};

  m_screen_analizer = new ScreenAnalizer(objetos);

  // FIXME: Descablear esta parte.
  objetos.push_back(&m_grid);
  m_perception_manager = new PerceptionManager(objetos);

  m_neural_manager = new NeuralManager(m_perception_manager);

  m_genetic_trainer = new GeneticTrainer(m_perception_manager, m_neural_manager);

  // FIXME: Descablear esta parte.
  //objetos.push_back(&m_grid);
  rect_t bounds = m_screen_analizer->getBounds();
  m_gameview = new GameView(bounds.width, bounds.height, objetos);

  m_input = new InputInterface(m_perception_manager);

  m_training_manager = new TrainingManager(m_input, m_perception_manager);

  m_iteracion = 0;

  // Iniciar m�dulos
  if (SDL_Init(SDL_INIT_VIDEO|SDL_INIT_TIMER) != 0) {
    std::cout << "Unable to initialize SDL2: " << SDL_GetError() << std::endl;
    return false;
  }

  if(TTF_Init() != 0) {
    std::cout << "Unable to initialize SDL2_TFF: " << TTF_GetError() << std::endl;
    return false;
  }

  // Iniciar otras cosas
  m_paused  = Config::getInstance().get_bool("app", "start_paused");
  m_running = true;
  m_gameview->onInit();

  switch(m_appOption){
    case RUN_BOT:
      m_input->set_unsupervised();
      m_neural_manager->onInit(m_arguments[0].c_str());
    break;
    case TRAIN_GENETIC:
      m_input->set_unsupervised();
      m_genetic_trainer->onInit(atoi(m_arguments[0].c_str()), atoi(m_arguments[1].c_str()), atoi(m_arguments[2].c_str()), atoi(m_arguments[3].c_str()), atoi(m_arguments[4].c_str()), atoi(m_arguments[5].c_str()));
      m_neural_manager->onInit();
      m_genetic_trainer->initGame();
    break;
    case TRAIN_SUPERVISED:
      // Iniciar entrenamiento supervisado
      m_input->set_supervised();
      m_training_manager->beginTraining();
    break;
    case TRAIN_NET:

    break;
  }

  return true;
}

bool App::onClose() {
  switch(m_appOption){
      case RUN_BOT:
        m_neural_manager->onClose();
      break;
      case TRAIN_GENETIC:
        m_genetic_trainer->onClose();
      break;
      case TRAIN_SUPERVISED:
        if (m_training_manager->inTraining())
          m_training_manager->endTraining( m_arguments[0].c_str() );
      break;
      case TRAIN_NET:break;
    }

  m_running = false;
  m_gameview->onClose();
  m_training_manager->onClose();


  // Cerrar módulos
  TTF_Quit();
  SDL_Quit();

  delete m_screen_analizer;
  delete m_gameview;
  delete m_input;
  delete m_neural_manager;
  delete m_training_manager;
  delete m_perception_manager;

  return true;
}

void App::onEvent() {
  while(SDL_PollEvent(&event)) {
    m_screen_analizer->onEvent(&event);
    m_input->onEvent(&event);

    // Botones especiales
    if(event.type == SDL_KEYDOWN) {
      if(event.key.keysym.scancode == SDL_SCANCODE_SPACE) {
        m_paused = !m_paused;
      }
      if(m_appOption == TRAIN_SUPERVISED and event.key.keysym.scancode == SDL_SCANCODE_T) {
        if (m_training_manager->inTraining())
          m_training_manager->endTraining( m_arguments[0].c_str() );
        else
          m_training_manager->beginTraining();
      }
    }
    // Salir
    if(event.type == SDL_QUIT) {
      exit();
    }
  }
}

void App::onLoop() {
  if(m_paused) return;

  m_screen_analizer->onLoop();
  m_perception_manager->onLoop();
  m_neural_manager->onLoop();

  if(m_appOption == RUN_BOT or m_appOption == TRAIN_GENETIC) {
    if (m_input->mustMove()) {
      switch (m_neural_manager->movement()) {
        case arriba: m_input->move(Movements::UP); break;
        case abajo: m_input->move(Movements::DOWN); break;
        case derecha: m_input->move(Movements::RIGHT); break;
        case izquierda: m_input->move(Movements::LEFT); break;
        case error: m_input->move(Movements::NONE); break;
      }
    }
  }

  m_input->onLoop();
  if(m_appOption == TRAIN_SUPERVISED and !m_paused)
    m_training_manager->onLoop();

  // Comprobar movimiento de la red
  //std::cout << m_neural_manager->movement() << std::endl;

  // Comprobar coordenadas
  //std::cout << "RCoords: " << m_pacman.getX()           << ", " << m_pacman.getY() << std::endl;
  //std::cout << " Coords: " << m_pacman.getGridX(m_grid) << ", " << m_pacman.getGridY(m_grid) << std::endl;
}


void App::onRender() {
  m_screen_analizer->onRender();
  m_gameview->onRender();
  m_input->onRender();
}

int App::onExecute(AppOptions appOpt, arguments_t args) {
  m_appOption = appOpt;
  m_arguments = args;
  if(!onInit())
    return ERROR_CODE;

  Timer t;
  t.start();

  while(m_running) {
    onEvent();
    onLoop();
    onRender();

    if(m_appOption != RUN_BOT and m_appOption != TRAIN_SUPERVISED and !m_paused and m_screen_analizer->is_game_over()) {
      onGameOver();
    }

    // Esperar un tiempo
    SDL_Delay(1);
  }

  if(!onClose())
    return ERROR_CODE;

  return SUCCESS_CODE;
}

void App::onGameOver() {
  // TODO Poner el código conveniente.
  //std::cout << "Game over!" << std::endl;
  m_screen_analizer->reset_game_over(); // <- Reiniciar flag del screen analizer

  m_genetic_trainer->endGame();

  m_perception_manager->resetGame();

    // Reiniciar partida!
  // Instantáneo (como el colacao).
  reset_game();
  // Esperar un par de segundos (lag, conexión, etc).
  Timer t(true);
  while(t.get_ms() < START_GAME_DELAY) {
    onEvent();
    SDL_Delay(1);
  }

  // E iniciar la nueva partida
  start_game();
  // Y esperar
  t.start();
  while(t.get_ms() < START_DELAY) {
    onEvent();
    SDL_Delay(1);
  }
}

void App::reset_game() {
  m_input->click_relative(m_screen_analizer, RESET_BUTTON_REL_X, RESET_BUTTON_REL_Y);
  //m_input->press_key(RESET_KEY);
}

void App::start_game() {
  m_input->click_relative(m_screen_analizer, PLAY_BUTTON_REL_X, PLAY_BUTTON_REL_Y);
  m_input->click_relative(m_screen_analizer, PLAY_BUTTON_REL_X, PLAY_BUTTON_REL_Y);
}

void App::exit() {
  m_running = false;
}

App& App::getInstance() {
  static App instance; // Se asegura que la clase es �nica, y ser� borrada.

  return instance;
}

