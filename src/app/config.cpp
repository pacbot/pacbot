/*
 * config.cpp
 *
 *  Created on: 30/12/2014
 *      Author: Dani
 */

#include "app/config.hpp"

Config& Config::getInstance() {
  static Config c;

  return c;
}

Config::Config(): m_reader(CONFIG_FILE) {

}

Config::~Config() {

}

std::string Config::get_str(std::string section, std::string name) {
  return m_reader.Get(section, name, DEFAULT_STR);
}

int Config::get_int(std::string section, std::string name) {
  return m_reader.GetInteger(section, name, DEFAULT_INT);
}

color_t Config::get_color(std::string section, std::string name) {
  color_t c;
  c.parse( m_reader.Get(section, name, DEFAULT_COLOR_STR) );

  return c;
}

double Config::get_double(std::string section, std::string name) {
  return m_reader.GetReal(section, name, DEFAULT_DOUBLE);
}

bool Config::get_bool (std::string section, std::string name) {
  return m_reader.GetBoolean(section, name, DEFAULT_BOOL);
}
