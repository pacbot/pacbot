/*
 * training_manager.cpp
 *
 *  Created on: 13/01/2015
 *      Author: Cristo
 */

#include "app/training_manager.hpp"

TrainingManager::TrainingManager(InputInterface* m_i, PerceptionManager* p_m) {

  m_in_training = false;
  m_inited = true;

  m_input = m_i;
  m_perception_manager = p_m;

}

void TrainingManager::beginTraining() {

  m_training.clear();

  m_training.append_node(m_training.allocate_node(node_element, "Training"));

  m_in_training = true;
}

void TrainingManager::onClose() {

  m_training.clear();
}

void TrainingManager::onLoop() {
  if(!m_inited)
    return;
  if(m_in_training) {
    addPattern();
  }
}

void TrainingManager::endTraining(const char* save_file) {

  m_in_training = false;

  char* file_training = new char[SIZE_FILE_NAME];
  strcpy(file_training, save_file);
  exportTraining(file_training);
  delete [] file_training;

  m_perception_manager->resetGame();
}

void TrainingManager::addPattern() {

  neuron_t* perceptions = m_perception_manager->getPerceptions();
  Movements movement = m_input->getLastKeyPressed();

  char* num_str;
  xml_node<> *node_up, *node_down, *node_right, *node_left;

  if (movement == Movements::NONE || perceptions[(int)movement] == 0.0)
    return;

  xml_node<>* node_p = m_training.allocate_node(node_element, "Pattern");

  /* Percepciones del patrón */
  xml_node<>* node_input = m_training.allocate_node(node_element, "Input");

  node_p->append_node(node_input);

  xml_node<>* node_movement = m_training.allocate_node(node_element, "Movement");
  xml_node<>* node_ghosts = m_training.allocate_node(node_element, "Ghosts");
  xml_node<>* node_dots_distance = m_training.allocate_node(node_element, "Dots_Distance");

  node_input->append_node(node_movement);
  node_input->append_node(node_ghosts);
  node_input->append_node(node_dots_distance);

  num_str = new char[SIZE_FLOAT];
  sprintf(num_str, "%f", perceptions[0]);
  node_up = m_training.allocate_node(node_element, "Up", num_str);


  num_str = new char[SIZE_FLOAT];
  sprintf(num_str, "%f", perceptions[1]);
  node_down = m_training.allocate_node(node_element, "Down", num_str);


  num_str = new char[SIZE_FLOAT];
  sprintf(num_str, "%f", perceptions[2]);
  node_right = m_training.allocate_node(node_element, "Right", num_str);


  num_str = new char[SIZE_FLOAT];
  sprintf(num_str, "%f", perceptions[3]);
  node_left = m_training.allocate_node(node_element, "Left", num_str);


  node_movement->append_node(node_up);
  node_movement->append_node(node_down);
  node_movement->append_node(node_right);
  node_movement->append_node(node_left);

  xml_node<>* node_distance = m_training.allocate_node(node_element, "Distance");
  xml_node<>* node_vulnerability = m_training.allocate_node(node_element, "Vulnerability");

  node_ghosts->append_node(node_distance);
  node_ghosts->append_node(node_vulnerability);

  xml_node<>* node_ghost_red = m_training.allocate_node(node_element, "Red");
  xml_node<>* node_ghost_pink = m_training.allocate_node(node_element, "Pink");
  xml_node<>* node_ghost_cyan = m_training.allocate_node(node_element, "Cyan");
  xml_node<>* node_ghost_orange = m_training.allocate_node(node_element, "Orange");

  node_distance->append_node(node_ghost_red);
  node_distance->append_node(node_ghost_pink);
  node_distance->append_node(node_ghost_cyan);
  node_distance->append_node(node_ghost_orange);

  num_str = new char[SIZE_FLOAT];
  sprintf(num_str, "%f", perceptions[4]);
  node_up = m_training.allocate_node(node_element, "Up", num_str);


  num_str = new char[SIZE_FLOAT];
  sprintf(num_str, "%f", perceptions[5]);
  node_down = m_training.allocate_node(node_element, "Down", num_str);


  num_str = new char[SIZE_FLOAT];
  sprintf(num_str, "%f", perceptions[6]);
  node_right = m_training.allocate_node(node_element, "Right", num_str);


  num_str = new char[SIZE_FLOAT];
  sprintf(num_str, "%f", perceptions[7]);
  node_left = m_training.allocate_node(node_element, "Left", num_str);


  node_ghost_red->append_node(node_up);
  node_ghost_red->append_node(node_down);
  node_ghost_red->append_node(node_right);
  node_ghost_red->append_node(node_left);

  num_str = new char[SIZE_FLOAT];
  sprintf(num_str, "%f", perceptions[8]);
  node_up = m_training.allocate_node(node_element, "Up", num_str);


  num_str = new char[SIZE_FLOAT];
  sprintf(num_str, "%f", perceptions[9]);
  node_down = m_training.allocate_node(node_element, "Down", num_str);


  num_str = new char[SIZE_FLOAT];
  sprintf(num_str, "%f", perceptions[10]);
  node_right = m_training.allocate_node(node_element, "Right", num_str);


  num_str = new char[SIZE_FLOAT];
  sprintf(num_str, "%f", perceptions[11]);
  node_left = m_training.allocate_node(node_element, "Left", num_str);


  node_ghost_pink->append_node(node_up);
  node_ghost_pink->append_node(node_down);
  node_ghost_pink->append_node(node_right);
  node_ghost_pink->append_node(node_left);

  num_str = new char[SIZE_FLOAT];
  sprintf(num_str, "%f", perceptions[12]);
  node_up = m_training.allocate_node(node_element, "Up", num_str);


  num_str = new char[SIZE_FLOAT];
  sprintf(num_str, "%f", perceptions[13]);
  node_down = m_training.allocate_node(node_element, "Down", num_str);


  num_str = new char[SIZE_FLOAT];
  sprintf(num_str, "%f", perceptions[14]);
  node_right = m_training.allocate_node(node_element, "Right", num_str);


  num_str = new char[SIZE_FLOAT];
  sprintf(num_str, "%f", perceptions[15]);
  node_left = m_training.allocate_node(node_element, "Left", num_str);


  node_ghost_cyan->append_node(node_up);
  node_ghost_cyan->append_node(node_down);
  node_ghost_cyan->append_node(node_right);
  node_ghost_cyan->append_node(node_left);

  num_str = new char[SIZE_FLOAT];
  sprintf(num_str, "%f", perceptions[16]);
  node_up = m_training.allocate_node(node_element, "Up", num_str);


  num_str = new char[SIZE_FLOAT];
  sprintf(num_str, "%f", perceptions[17]);
  node_down = m_training.allocate_node(node_element, "Down", num_str);


  num_str = new char[SIZE_FLOAT];
  sprintf(num_str, "%f", perceptions[18]);
  node_right = m_training.allocate_node(node_element, "Right", num_str);


  num_str = new char[SIZE_FLOAT];
  sprintf(num_str, "%f", perceptions[19]);
  node_left = m_training.allocate_node(node_element, "Left", num_str);


  node_ghost_orange->append_node(node_up);
  node_ghost_orange->append_node(node_down);
  node_ghost_orange->append_node(node_right);
  node_ghost_orange->append_node(node_left);


  num_str = new char[SIZE_FLOAT];
  sprintf(num_str, "%f", perceptions[20]);
  node_up = m_training.allocate_node(node_element, "Red", num_str);


  num_str = new char[SIZE_FLOAT];
  sprintf(num_str, "%f", perceptions[21]);
  node_down = m_training.allocate_node(node_element, "Pink", num_str);


  num_str = new char[SIZE_FLOAT];
  sprintf(num_str, "%f", perceptions[22]);
  node_right = m_training.allocate_node(node_element, "Cyan", num_str);


  num_str = new char[SIZE_FLOAT];
  sprintf(num_str, "%f", perceptions[23]);
  node_left = m_training.allocate_node(node_element, "Orange", num_str);


  node_vulnerability->append_node(node_up);
  node_vulnerability->append_node(node_down);
  node_vulnerability->append_node(node_right);
  node_vulnerability->append_node(node_left);

  xml_node<>* node_small_dots = m_training.allocate_node(node_element, "Small");
  xml_node<>* node_big_dots = m_training.allocate_node(node_element, "Big");

  node_dots_distance->append_node(node_small_dots);
  node_dots_distance->append_node(node_big_dots);

  num_str = new char[SIZE_FLOAT];
  sprintf(num_str, "%f", perceptions[24]);
  node_up = m_training.allocate_node(node_element, "Up", num_str);


  num_str = new char[SIZE_FLOAT];
  sprintf(num_str, "%f", perceptions[25]);
  node_down = m_training.allocate_node(node_element, "Down", num_str);


  num_str = new char[SIZE_FLOAT];
  sprintf(num_str, "%f", perceptions[26]);
  node_right = m_training.allocate_node(node_element, "Right", num_str);


  num_str = new char[SIZE_FLOAT];
  sprintf(num_str, "%f", perceptions[27]);
  node_left = m_training.allocate_node(node_element, "Left", num_str);


  node_small_dots->append_node(node_up);
  node_small_dots->append_node(node_down);
  node_small_dots->append_node(node_right);
  node_small_dots->append_node(node_left);

  num_str = new char[SIZE_FLOAT];
  sprintf(num_str, "%f", perceptions[28]);
  node_up = m_training.allocate_node(node_element, "Up", num_str);


  num_str = new char[SIZE_FLOAT];
  sprintf(num_str, "%f", perceptions[29]);
  node_down = m_training.allocate_node(node_element, "Down", num_str);


  num_str = new char[SIZE_FLOAT];
  sprintf(num_str, "%f", perceptions[30]);
  node_right = m_training.allocate_node(node_element, "Right", num_str);


  num_str = new char[SIZE_FLOAT];
  sprintf(num_str, "%f", perceptions[31]);
  node_left = m_training.allocate_node(node_element, "Left", num_str);


  node_big_dots->append_node(node_up);
  node_big_dots->append_node(node_down);
  node_big_dots->append_node(node_right);
  node_big_dots->append_node(node_left);



  /* Movimiento del patrón */
  xml_node<>* node_output = m_training.allocate_node(node_element, "Output");

  num_str = new char[SIZE_FLOAT];
  sprintf(num_str, "%f", movement == Movements::UP? 1.0 : (perceptions[(int)Movements::UP] == 1.0? 0.5 : 0.0));
  node_up = m_training.allocate_node(node_element, "Up", num_str);


  num_str = new char[SIZE_FLOAT];
  sprintf(num_str, "%f", movement == Movements::DOWN? 1.0 : (perceptions[(int)Movements::DOWN] == 1.0? 0.5 : 0.0));
  node_down = m_training.allocate_node(node_element, "Down", num_str);


  num_str = new char[SIZE_FLOAT];
  sprintf(num_str, "%f", movement == Movements::RIGHT? 1.0 : (perceptions[(int)Movements::RIGHT] == 1.0? 0.5 : 0.0));
  node_right = m_training.allocate_node(node_element, "Right", num_str);


  num_str = new char[SIZE_FLOAT];
  sprintf(num_str, "%f", movement == Movements::LEFT? 1.0 : (perceptions[(int)Movements::LEFT] == 1.0? 0.5 : 0.0));
  node_left = m_training.allocate_node(node_element, "Left", num_str);


  node_output->append_node(node_up);
  node_output->append_node(node_down);
  node_output->append_node(node_right);
  node_output->append_node(node_left);

  node_p->append_node(node_output);

  (m_training.first_node())->append_node(node_p);
}

void TrainingManager::exportTraining(char* training_file) {

  fstream os;
  os.open(training_file, ios_base::out);

  if(os.is_open()) {

    os << m_training;

    os.close();
  }
  else
      cerr << "Error: Escritura incorrecta del fichero " << training_file << endl;

}


