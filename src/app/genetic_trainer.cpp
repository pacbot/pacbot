/*
 * genetic_trainer.cpp
 *
 *  Created on: 17/01/2015
 *      Author: Cristo
 */

#include "app/genetic_trainer.hpp"
#include "app/app.hpp"

GeneticTrainer::GeneticTrainer(PerceptionManager* p_m, NeuralManager* n_m) {

  m_perception_manager = p_m;
  m_neural_manager = n_m;

  m_inited = false;
}

bool GeneticTrainer::onInit(int p1, int p2, int p3, int p4, int p5, int p6) {

  N_POBLACION = p1;
  N_GENERACIONES = p2;
  N_SELECCIONADOS = p3;
  N_MIN_MUTACIONES = p4, N_MAX_MUTACIONES = p5;
  N_JUEGOS_BOT = p6;

  m_beneficios = new int[N_POBLACION];

  m_poblacion = new MultiLayerPerceptron*[N_POBLACION];
  for (int i = 0; i < N_POBLACION; i++) {
    m_poblacion[i] = new MultiLayerPerceptron();

    /*char* file = new char[SIZE_FILE_NAME];
    strcpy(file, SSTR("assets/genetic/net_init_" << i << ".xml").c_str());
    m_poblacion[i]->importNet(file);*/
    m_poblacion[i]->initWeightsRandom();
    m_beneficios[i] = 0;
  }

  m_inited = true;

  m_generacion = 0;
  m_paisano = 0;
  m_juego_paisano = 0;

  return m_inited;
}


void GeneticTrainer::onLoop() {

  if(!m_inited)
    return;

}

void GeneticTrainer::onClose() {

  delete [] m_beneficios;

  for (int i = 0; i < N_POBLACION; i++)
    delete m_poblacion[i];
  delete [] m_poblacion;
}

void GeneticTrainer::initGame() {
  m_neural_manager->setNet(m_poblacion[m_paisano]);
}

void GeneticTrainer::endGame() {
   m_beneficios[m_paisano] += m_perception_manager->getPuntuacion();
   cout << "Generacion: " << m_generacion << "\tJugador: " << m_paisano << "\tPuntuacion promedio: " << m_beneficios[m_paisano]/(m_juego_paisano + 1.0) << endl;

  ++m_juego_paisano;

  if (m_juego_paisano == N_JUEGOS_BOT) {
    m_juego_paisano = 0;
    ++m_paisano;

    if (m_paisano == N_POBLACION) {
      m_paisano = 0;
      avanzarGeneracion();
    }
    else
      initGame();
  }
}


void GeneticTrainer::avanzarGeneracion() {

  MultiLayerPerceptron** nuevaPoblacion = new MultiLayerPerceptron*[N_POBLACION];
  for (int i = 0; i < N_POBLACION; i++) {
    nuevaPoblacion[i] = new MultiLayerPerceptron();
    m_beneficios[i] /= N_JUEGOS_BOT;
  }

  /* Ordenación de los individuos */
  for (int i = 0; i < (N_POBLACION - 1); ++i) {

    int max_index = i;
    int max_value = m_beneficios[i];

    for (int j = i + 1; j < N_POBLACION; ++j) {
      if (m_beneficios[j] > max_value) {
        max_value = m_beneficios[j];
        max_index = j;
      }
    }

    swap(m_beneficios[i], m_beneficios[max_index]);
    swap(m_poblacion[i], m_poblacion[max_index]);
  }

  /* Algoritmo Genético */
  for(int i = 0; i < (N_POBLACION/2); ++i){

    int padre = randBetween(0, N_SELECCIONADOS - 1);
    int madre;
    do {
      madre = randBetween(0, N_SELECCIONADOS - 1);
    } while(madre == padre);

    WeightMatrix*** combinacion = mergeNets(m_poblacion[padre], m_poblacion[madre]);

    nuevaPoblacion[i*2]->loadWeights(combinacion[0]);
    nuevaPoblacion[i*2 + 1]->loadWeights(combinacion[1]);
    delete [] combinacion;

    nuevaPoblacion[i*2]->mutateNet(N_MIN_MUTACIONES, N_MAX_MUTACIONES);
    nuevaPoblacion[i*2 + 1]->mutateNet(N_MIN_MUTACIONES, N_MAX_MUTACIONES);
  }

  exportPoblacion();

  for (int i = 0; i < N_POBLACION; ++i) {
    delete m_poblacion[i];
    m_beneficios[i] = 0;
  }
  delete [] m_poblacion;

  m_poblacion = nuevaPoblacion;
  ++m_generacion;

  if (m_generacion >= N_GENERACIONES)
    App::getInstance().exit();
  else
    initGame();

}

void GeneticTrainer::exportPoblacion() {

  char* file = NULL;

  for (int i = 0; i < N_POBLACION; ++i) {
    file = new char[SIZE_FILE_NAME];
    strcpy(file, SSTR("assets/genetic/net_" << m_generacion << "_" << i << "___" << m_beneficios[i] << ".xml").c_str());
    m_poblacion[i]->exportNet(file);
  }
}

WeightMatrix*** GeneticTrainer::mergeNets(MultiLayerPerceptron* padre, MultiLayerPerceptron* madre) {

  WeightMatrix*** hijos = new WeightMatrix**[2];
  hijos[0] = new WeightMatrix*[N_WEIGHTS_LAYERS];
  hijos[1] = new WeightMatrix*[N_WEIGHTS_LAYERS];

  for (int i = 0; i < N_WEIGHTS_LAYERS; ++i) {

    hijos[0][i] = new WeightMatrix(N_NEURONS[i], N_NEURONS[i + 1], true);
    hijos[1][i] = new WeightMatrix(N_NEURONS[i], N_NEURONS[i + 1], true);

    int min = randBetween(0, N_NEURONS[i] - 2);
    int max = randBetween(min, N_NEURONS[i] - 1);

    for (int j = 0; j < N_NEURONS[i]; ++j) {

      for (int k = 0; k < N_NEURONS[i + 1]; ++k) {
        if (j < min || j > max) {
          hijos[0][i]->setWeight(j, k, padre->getWeight(i, j, k));
          hijos[1][i]->setWeight(j, k, madre->getWeight(i, j, k));
        }
        else {
          hijos[0][i]->setWeight(j, k, madre->getWeight(i, j, k));
          hijos[1][i]->setWeight(j, k, padre->getWeight(i, j, k));
        }
      }
    }

  }

  return hijos;
}
