/*
 * globalNN.hpp
 *
 *  Created on: 24/11/2014
 *      Author: Cristo
 */

#ifndef GLOBALNN_HPP_
#define GLOBALNN_HPP_

#define M_E 2.71828182845904523536

#include "global.hpp"

using namespace std;
using namespace rapidxml;

typedef float neuron_t;
typedef double weight_t;

enum output_t {
  arriba = 0,
  abajo,
  derecha,
  izquierda,
  error
};

const extern int DECIMALS;

const extern int SIZE_FILE_NAME, SIZE_FLOAT;

const extern weight_t LOWER_WEIGHT, HIGHER_WEIGHT;
const extern int N_HIDDEN_LAYERS, N_NEURON_LAYERS, N_WEIGHTS_LAYERS;
const extern int N_NEURONS[];

const extern neuron_t ALFA;

extern weight_t randWeightBetween(weight_t first, weight_t second);
extern neuron_t sigmoid(neuron_t x);
extern neuron_t derivative_sigmoid(neuron_t x);
extern neuron_t identity(neuron_t x);

#endif /* GLOBALNN_HPP_ */
