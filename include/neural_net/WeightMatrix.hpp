/*
 * WeightMatrix.hpp
 *
 *  Created on: 24/11/2014
 *      Author: Cristo
 */

#ifndef WEIGHTMATRIX_HPP_
#define WEIGHTMATRIX_HPP_

#include "neural_net/globalNN.hpp"

/**
 * @class WeightMatrix
 * @brief Representa a una capa de sinapsis (con pesos asociados) que conecta dos capas de neuronas
 *
 * Capa de sinapsis representada mediante una matriz de pesos, de manera que el peso ('i', 'j'), representa el peso de la
 * sinapsis que conecta a la neurona 'i' de la primera capa (entrada), con la neurona 'j' de la segunda capa (salida)
 */
class WeightMatrix {

  private:
    int inputSize;
    int outputSize;
    weight_t** weights;

  public:
    WeightMatrix(int x, int y, bool allocate  = false);
    virtual ~WeightMatrix();

    weight_t* getInputWeights(int targetNeuron);
    weight_t* getOutputWeights(int sourceNeuron);

    weight_t getWeight(int ni, int no) { return weights[ni][no]; }
    void setWeight(int ni, int no, weight_t w) { weights[ni][no] = w; }

    void initWeightsRandom();
    void initWeights(weight_t** w);

    void adjustWeight(int ni, int no, weight_t weight_increment) { weights[ni][no] += weight_increment; }

    void mutateWeight(int ni, int no);

  private:
    void allocateMemory();
    void freeMemory();

};

#endif /* WEIGHTMATRIX_HPP_ */
