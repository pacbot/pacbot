/*
 * Neuron.hpp
 *
 *  Created on: 24/11/2014
 *      Author: Cristo
 */

#ifndef NEURON_HPP_
#define NEURON_HPP_

#include "neural_net/globalNN.hpp"

/**
 * @class Neuron
 * @brief Representa a una neurona
 *
 * Neurona de una determinada capa de una red neuronal
 */
class Neuron {

  private:
    neuron_t inputValue;
    neuron_t outputValue;
    neuron_t sensitivity;

  public:
    Neuron();
    virtual ~Neuron();

    neuron_t getInputValue();
    neuron_t getOutputValue();
    neuron_t getSensitivity();

    void init(neuron_t input);
    void computeOutput(bool first_layer);
    void computeOutputSensitivity(neuron_t target_value);
    void computeHiddenSensitivity(neuron_t summation);

};

#endif /* NEURON_HPP_ */
