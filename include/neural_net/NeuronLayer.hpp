/*
 * NeuronLayer.hpp
 *
 *  Created on: 24/11/2014
 *      Author: Cristo
 */

#ifndef NEURONLAYER_HPP_
#define NEURONLAYER_HPP_

#include "neural_net/globalNN.hpp"
#include "neural_net/Neuron.hpp"

/**
 * @class NeuronLayer
 * @brief Representa a una capa de neuronas
 *
 * Capa de neuronas de una red neuronal, constituida por una determinada cantidad de neuronas
 */
class NeuronLayer {

  private:
    int lSize;
    Neuron* layer;

  public:
    NeuronLayer(int size);
    virtual ~NeuronLayer();

    neuron_t* getOutput();
    neuron_t* getSensitivity();

    void setInput(neuron_t* value);
    void computeOutput(bool first_layer = false);
    void computeOutputLayerSensitivity(neuron_t* pattern);
    void computeHiddenLayerSensitivity(neuron_t* pattern);

};

#endif /* NEURONLAYER_HPP_ */
