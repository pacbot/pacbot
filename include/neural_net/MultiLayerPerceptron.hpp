/*
 * MultiLayerPerceptron.hpp
 *
 *  Created on: 24/11/2014
 *      Author: Cristo
 */

#ifndef MULTILAYERPERCEPTRON_HPP_
#define MULTILAYERPERCEPTRON_HPP_

#include "neural_net/globalNN.hpp"
#include "neural_net/NeuronLayer.hpp"
#include "neural_net/WeightMatrix.hpp"

/**
 * @class MultiLayerPerceptron
 * @brief Representa a un perceptrón multicapa
 * @see N_HIDDEN_LAYERS determina el nº de capas ocultas de la red y N_NEURONS indica el nº de neuronas de cada capa
 *
 * Perceptrón multicapa, tipo de red neuronal formada por una capa de entrada, una de salida y al menos una
 * capa oculta, con propagación hacia adelante. Se encuentra totalmente conectado, con valores de entradas
 * flotantes entre 0.0 y 1.0, y valores de pesos también flotantes sin limitación.
 *
 * Se emplea la función sigmoide para obtener la salida de las neuronas a partir de su entrada neta (excepto para
 * la capa de entrada, que se emplea la identidad), sumatorio de los pesos por las salidas de las neuronas para la
 * propagación, y el algoritmo de retropropagación para el aprendizaje
 */
class MultiLayerPerceptron {

  private:
    NeuronLayer** layers;
    WeightMatrix** weights;

  public:
    MultiLayerPerceptron();
    virtual ~MultiLayerPerceptron();

    void readInput(char* file);
    void setInput(neuron_t* input);

    void importNet(char* file);
    void loadWeights(weight_t*** weights_layers);
    void loadWeights(WeightMatrix** weight_layer);
    void initWeightsRandom();

    void mutateNet(int min_mutaciones, int max_mutaciones);
    weight_t getWeight(int layer, int ni, int no) { return weights[layer]->getWeight(ni, no); }

    void exportNet(char* file);

    void computeOutput();
    neuron_t* getOutput();
    output_t getBetterMovement();

    void simpleTraining(neuron_t* target_output);
    void completeTraining(char* training_file, char* weights_file = NULL);

  private:
    void traverseInputTree(xml_node<>* node, neuron_t *input, int &n_input);

    neuron_t* computeLayerInput(int n_layer);
    void computeNetSensitivity(neuron_t* pattern);
    void adjustWeights();

};

#endif /* MULTILAYERPERCEPTRON_HPP_ */
