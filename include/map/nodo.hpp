/*
 * nodo.hpp
 *
 *  Created on: 26/11/2014
 *      Author: God
 *
 *  Descripci�n: Clase nodo que contendr� la informaci�n b�sica de cada casilla del tablero
 *
 */

#ifndef NODO_HPP_
#define NODO_HPP_

class CNodo {

private:
	bool coco;
	bool cocoG;
	CNodo** hijos;
	bool visitado;

	CNodo();
	CNodo(bool,bool);

public:
	~CNodo();
	bool getCoco();
	bool getCocoG();
	bool getVisitado();
	CNodo* getHijo(int);
	void setHijo(CNodo*, output_t);
	void setCoco(bool);
	void setCocoG(bool);
	void setVisitado(bool);

	int numeroHijos();
	void comer();

	static CNodo* CrearNodo(const char tipoNodo)
	{
		CNodo* nodo;

		if(tipoNodo == CASILLA_VACIA){
			nodo = new CNodo();
		}
		else if(tipoNodo == CASILLA_COCO){
			nodo = new CNodo(true,false);
		}
		else if(tipoNodo == CASILLA_COCO_GRANDE){
			nodo = new CNodo(false,true);
		}

		return nodo;
	}

};

#endif

/*NODO_HPP_*/

