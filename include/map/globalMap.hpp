/*
 * globalMap.hpp
 *
 *  Created on: 26/11/2014
 *      Author: God
 */

#ifndef GLOBALMAP_HPP_
#define GLOBALMAP_HPP_

#include <iostream>
#include <fstream>
#include <vector>

#include <stdlib.h>
#include <limits.h>

#include "neural_net/globalNN.hpp"

using namespace std;

extern const char* NOMBRE_FICHERO;
extern const int HIJOS;
extern const int FUERA_VISION;
extern const int GLOBAL;
extern const int VISION;
extern const int COCO, COCOG;

extern const char CASILLA_COCO;
extern const char CASILLA_COCO_GRANDE;
extern const char CASILLA_VACIA;

enum Objetivo {coco = 0, cocoGrande};


double conversor01(int x, int y);


#endif
/*GLOBALS_HPP_*/
