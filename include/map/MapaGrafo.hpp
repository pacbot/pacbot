/*
 * MapaGrafo.hpp
 *
 *  Created on: 26/11/2014
 *      Author: God
 */

#ifndef MAPAGRAFO_HPP_
#define MAPAGRAFO_HPP_

#include "map/globalMap.hpp"
#include "map/nodo.hpp"

class MapaGrafo{

private:
	CNodo* posActual;  // Posicion actual
	CNodo *** mapa;    // Matriz de nodos
	int numNodos;	   // Numero de nodos en el mapa
	int alto;
	int ancho;
	int puntuacion;

public:
	 MapaGrafo();
	 ~MapaGrafo();
	 //Inicializacion
	 void initGrafo(char**);
	 void unionNodos();
	 void initGame();

	 //Funciones percepcion
	 /**
	  * Devuelve la distancia del coco mas cercano en la dirección
	  * especificada (distancia global).
	  */
	 double cocoCercano(output_t);
	 /**
	  * Devuelve la distancia del coco grande mas cercano en la dirección
	  * especificada (en una distancia limitada).
	  */
	 double cocoGCercano(output_t);
	 /**
	  * Devuelve la distancia de la posicion y en la dirección
	  * especificada (distancia limitada).
	 */
	 double fantasmaCercano(int,int,output_t);
	  /**
	    * Devuelve si se permite el movimiento en la dirección indicada
	   */
	 bool movimientoPermitido(output_t);

   CNodo* getPos(int,int);

   int getPuntuacion(){ return puntuacion; }


   void move(int,int);




private:
   //Funciones utilidad
   bool buscar(CNodo*,Objetivo);
   int recorridoAmplitud(output_t, Objetivo);
   int recorridoAmplitud(output_t, int, int);
   void limpiarVisitados();
   void limpiarVisitados(int x, int y);
   void comer();

};

#endif
/*MAPAGRAFO_HPP_*/


