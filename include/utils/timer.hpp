/*
 * timer.hpp
 *
 *  Created on: 29/11/2014
 *      Author: Dani
 */

#ifndef TIMER_HPP_
#define TIMER_HPP_

#include "global.hpp"

/** @brief Temporizador
 *
 * Clase para calcular tiempos, entre otras cosas. */
class Timer
{
  private:
    static int s_ids; /**< Acumulador para identificadores. */

  private:
    int m_id; /**< Identificador del temporizador (�nico). */

    int m_startTicks; /**< Ticks d�nde se empez� el temporizador. */
    int m_pausedTicks; /**< Ticks d�nde se pauso el temporizador por �ltima vez. */

    bool m_paused;  /**< Indica si el temporizador est� pausado. */
    bool m_started;  /**< Indica si el temporizador ha empezado. */
  public:


    /** Constructor del temporizador.
     * @param start Si es true, empieza iniciando la cuenta, llamado a Timer::start().*/
    Timer(bool start = true);

    /** Iniciar el temporizador. */
    void start();
    /** Parar el temporizador. */
    void stop();
    /** Pausar el temporizador. */
    void pause();
    /** Reanudar el temporizador. */
    void resume();

    /** Obtener tiempo transcurrido por el temporizador.
     * @return Tiempo transcurrido en milisegundos.*/
    int    get_ms();
    /** Obtener tiempo transcurrido por el temporizador.
     * @return Tiempo transcurrido en segundos.*/
    double get_s();

    /** Getter de m_paused.
     * @return true si est� pausado, false en caso contrario. */
    bool paused();

    /** Getter de m_started.
     * @return true si se ha iniciado, false en caso contrario. */
    bool started();

    /** Pintar por consola el temporizador, de la format "[Timer:<m_id>]: <tiempo_ms>ms"; */
    void print();
};

#endif /* TIMER_HPP_ */
