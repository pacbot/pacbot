/*
 * console.hpp
 *
 *  Created on: 16/01/2015
 *      Author: Dani
 */

#ifndef CONSOLE_HPP_
#define CONSOLE_HPP_

class ConsoleUI {
  private:
    const char* DEFAULT_NET_FILE   = "assets/default_net.xml";
    const char* DEFAULT_TRAIN_FILE = "assets/default_train.xml";

    ConsoleUI();

    void run_bot();
    void train_genetic();
    void train_supervised();
    void train_net();

  public:
    static inline ConsoleUI& getInstance() {
      static ConsoleUI c;
      return c;
    }

    void start();
};



#endif /* CONSOLE_HPP_ */
