/*
 * training_manager.hpp
 *
 *  Created on: 13/01/2015
 *      Author: Cristo
 */

#ifndef TRAINING_MANAGER_HPP_
#define TRAINING_MANAGER_HPP_

#include "neural_net/globalNN.hpp"

#include "app/input_interface.hpp"
#include "app/perception_manager.hpp"

class TrainingManager {

  private:

    InputInterface* m_input;
    PerceptionManager* m_perception_manager;

    xml_document<> m_training;

    bool m_inited;
    bool m_in_training;

  public:
    /** Constructor.
     * @param objects Lista de objetos del juego. */
    TrainingManager(InputInterface* m_i, PerceptionManager* p_m);

    /** Función de inicialización. */
    void beginTraining();

    /** Función de cierre. */
    void onClose();

    /** Función de iteración. */
    void onLoop();

    /** Generar entrenamiento */
    void endTraining(const char* save_file);

    /** Getter del estado de entrenamiento */
    bool inTraining() { return m_in_training; }

  private:
    /** Adición de patrón de entrenamiento */
    void addPattern();

    /** Exportación del entrenamiento */
    void exportTraining(char* training_file);
};





#endif /* TRAINING_MANAGER_HPP_ */
