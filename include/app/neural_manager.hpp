/*
 * neural_manager.hpp
 *
 *  Created on: 11/01/2015
 *      Author: Cristo
 */

#ifndef NEURAL_MANAGER_HPP_
#define NEURAL_MANAGER_HPP_

#include "neural_net/MultiLayerPerceptron.hpp"
#include "app/perception_manager.hpp"
#include "game/pacman.hpp"
#include "game/ghost.hpp"

class NeuralManager {

  private:

    MultiLayerPerceptron* m_red;

    PerceptionManager* m_perception_manager;

    bool m_inited;

  public:
    /** Constructor.
     * @param objects Lista de objetos del juego. */
    NeuralManager(PerceptionManager* p_m);

    /** Iniciar manager. */
    bool onInit(const char* file_input_net = NULL);

    /** Cerrar manager. */
    void onClose();

    /** Función de iteración. */
    void onLoop();

    /** Getter del movimiento. */
    output_t movement() { return m_red->getBetterMovement(); }

    void setNet(MultiLayerPerceptron* red);

};

#endif /* NEURAL_MANAGER_HPP_ */
