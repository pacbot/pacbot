/*
 * debug_view.hpp
 *
 *  Created on: 29/11/2014
 *      Author: Dani
 */

#ifndef DEBUG_VIEW_HPP_
#define DEBUG_VIEW_HPP_

#include "global.hpp"
#include "game/object.hpp" // �?

/**
 * Clase para mostrar los elementos del juego, a modo de depuraci�n.
 */
class GameView {
  private:
    // Depuraci�n
    const int DEBUG_HEIGHT = 200; /**< Alto libre para depurar */
    const int DEBUG_WIDTH  = 0;   /**< Ancho libre para depurar */

    const char* DEFAULT_FONT_FILE = "assets/pixeldeb.ttf"; /**< Fuente por defecto (fichero ttf) */
    const int   DEFAULT_FONT_SIZE = 16; /**< Tama�o de fuente por defecto */

    // Texto
    const int       DEBUG_TEXT_OFFSET  = 110; /**< Offset para el contenido del texto */

    // Comecocos
    const int       DEBUG_PACMAN_X     = 10; /**< Posici�n X del texto de depuraci�n del comecocos */
    const int       DEBUG_PACMAN_Y     = 10; /**< Posici�n Y del texto de depuraci�n del comecocos */
    const SDL_Color DEBUG_PACMAN_COLOR = {255, 255, 255, 255}; /**< Color del texto de depuraci�n del comecocos */

    // Blinky
    const int       DEBUG_BLINKY_X     = 10; /**< Posici�n X del texto de depuraci�n de blinky */
    const int       DEBUG_BLINKY_Y     = 30; /**< Posici�n Y del texto de depuraci�n de blinky */
    const SDL_Color DEBUG_BLINKY_COLOR = {221, 0, 0, 255}; /**< Color del texto de depuraci�n de blinky */

    // Pinky
    const int       DEBUG_PINKY_X      = 10; /**< Posici�n X del texto de depuraci�n de pinky */
    const int       DEBUG_PINKY_Y      = 50; /**< Posici�n Y del texto de depuraci�n de pinky */
    const SDL_Color DEBUG_PINKY_COLOR  = {255, 153, 153}; /**< Color del texto de depuraci�n de pinky */

    // Inky
    const int       DEBUG_INKY_X       = 10; /**< Posici�n X del texto de depuraci�n de inky */
    const int       DEBUG_INKY_Y       = 70; /**< Posici�n Y del texto de depuraci�n de inky */
    const SDL_Color DEBUG_INKY_COLOR   = {102, 255, 255}; /**< Color del texto de depuraci�n de inky */

    // Clyde
    const int       DEBUG_CLYDE_X      = 10; /**< Posici�n X del texto de depuraci�n de clyde */
    const int       DEBUG_CLYDE_Y      = 90; /**< Posici�n Y del texto de depuraci�n de clyde */
    const SDL_Color DEBUG_CLYDE_COLOR  = {255, 153, 0}; /**< Color del texto de depuraci�n de clyde */

    // Estados del juego (pausa, etc).
    const int       DEBUG_PAUSE_X = 10; /**< Posici�n X del texto de depuraci�n de pausa */
    const int       DEBUG_PAUSE_Y = 180; /**< Posici�n Y del texto de depuraci�n de pausa */
    const SDL_Color DEBUG_PAUSE_COLOR = {255, 0, 0, 255}; /**< Color del texto de depuraci�n de pausa */
    const char*     DEBUG_PAUSE = "PAUSED"; /**< Texto de pausa. */

  private:
    // Lista de objetos a representar.
    std::vector<Object*> m_objects; /**< Lista de objetos a representar */
    Grid* m_grid; /**< Rejilla */

    SDL_Window*   m_window;     /**< Ventana SDL */
    SDL_GLContext m_GLcontext;  /**< Contexto openGL */

    int m_width;  /**< Ancho de ventana */
    int m_height; /**< Alto de ventana */

    TTF_Font* m_default_font; /**< Fuente por defecto */

  public:
    /** Constructor
     * @param m_objects Lista de objetos a dibujar */
    GameView(int width, int height, std::vector<Object*> objects);

    /** Funci�n de iniciado.
     * @return Estado de ejecuci�n (true -> OK, false -> caso contrario). */
    bool onInit();

    /** Funci�n de cerrado. */
    void onClose();

    /** Funci�n de renderizado. */
    void onRender();

  protected:
    /** Redenrizado (depuraci�n). */
    void onDebugRender();

    /** Renderizar texto
     * @param text Texto a dibujar.
     * @param font Fuente (de SDL2_TTF).
     * @param color Color del texto.
     * @param x Posici�n x.
     * @param y Posici�n y.
     */
    void drawText(const char* text, TTF_Font* font, SDL_Color color, int x = 0, int y = 0);
};



#endif /* DEBUG_VIEW_HPP_ */
