/*
 * screen_analizer.hpp
 *
 *  Created on: 25/11/2014
 *      Author: Dani
 */

#ifndef SCREEN_ANALIZER_HPP_
#define SCREEN_ANALIZER_HPP_

#include <string>
#include <windows.h>

#include "game/pacman.hpp"
#include "game/ghost.hpp"

/**
 * Analizador de pantalla.
 *
 * Clase para buscar los elementos en pantalla del juego.
 */
class ScreenAnalizer {
  private:
    // Constantes de an�lisis
    int MIN_X; /**< Factor de ventana X de punto inicial X de b�squeda. @warning Si usar. */
    int MIN_Y; /**< Factor de ventana Y de punto inicial Y de b�squeda. @warning Si usar.  */

    int MAX_X; /**< Factor de ventana X de punto final X de b�squeda. @warning Si usar.  */
    int MAX_Y; /**< Factor de ventana Y de punto final Y de b�squeda. @warning Si usar.  */

    int INC_X; /**< Incremento X en la b�squeda. */
    int INC_Y; /**< Incremento Y en la b�squeda. */

    const static int BORDER_WIDTH = 2;  /**< Ancho del borde del rect�ngulo del �rea de an�lisis. */

    int GAME_OVER_X;                    /**< Posici�n X de la detecci�n del final de partida. */
    int GAME_OVER_Y;                    /**< Posici�n Y de la detecci�n del final de partida. */
    int GAME_OVER_WIDTH;                /**< Ancho de la detecci�n del final de partida. */
    int GAME_OVER_HEIGHT;               /**< Alto de la detecci�n del final de partida. */
    color_t GAME_OVER_COLOR;            /**< Color de la detecci�n del final de partida. */
    double GAME_OVER_COLOR_ERROR;       /**< Porcentaje de error del color de la detecci�n del final de partida. */
    double GAME_OVER_SEARCH_PERCENT;    /**< Porcentaje de la detecci�n del final de partida. */

  private:
    // Elementos de juego
    Pacman* m_pacman; /**< Un pacman (jugador). Referencia externa. */
    Ghost*  m_blinky_red;   /**< Fantasma rojo. */
    Ghost*  m_pinky_pink;   /**< Fantasma rosa. */
    Ghost*  m_inky_blue;    /**< Fantasma azul. */
    Ghost*  m_clyde_orange; /**< Fantasma naranja. */
    // Cocos, grafo...

    // Captura de pantalla a analizar (buffer).
    HBITMAP m_h_screenshot; /**< Handler de captura de pantalla a analizar (buffer). */
      PBITMAPINFO m_bitmap_info; /**< Informaci�n del bmp de la captura de pantalla */
      BITMAP m_bitmap; /**< Struct para el bitmap */
    rect_t  m_bounds;       /**< Regi�n de inter�s **/
    // ...

    bool m_inited; /**< Sistema iniciado (regi�n seleccionada). */
    bool m_draw; /**< Saber si dibujar o no el marco de la pantalla. */

    bool m_game_over; /**< Saber si se ha terminado la partida (para reiniciar). */

  private:
    /** Procesar imagen */
    void process_screenshot();

    /** Comprobar si ha terminado la partida.
     * @param pixels Array de p�xeles a analizar.
     * @param width Ancho del buffer de p�xeles.
     * @param height Alto del buffer de p�xeles.
     * @return true si ha terminado la partida. false en caso contrario. */
    bool game_over(BYTE *pixels, int width, int height);

    /** Realizar captura de pantalla */
    void captureScreenShot();

    /** Crear mapa de bits a partir de un handler */
    PBITMAPINFO createBitmapInfo();

    void reset_objets();

    /** Guardar la imagen actual a fichero (debug)
     * @param hDC Handle de ventana (escritorio)
     * @param file Nombre del fichero objetivo. */
    bool screenshotToFile(HDC hDC, std::string file); // http://msdn.microsoft.com/en-us/library/windows/desktop/dd145119(v=vs.85).aspx

  public:
    /** Constructor.
     * @param objects Lista de objetos del juego. */
    ScreenAnalizer(std::vector<Object*> objects);


    /** Funci�n de interaci�n. */
    void onLoop();

    /** Funci�n de dibujado. */
    void onRender();

    /** Eventos. */
    void onEvent(SDL_Event* event);
    // ...

    /** Getter de las dimensiones */
    rect_t getBounds();

    /** Reiniciar game over. */
    inline void reset_game_over() {
      m_game_over = false;
    }

    /** Getter de m_game_over. */
    inline bool is_game_over() {
      return m_game_over;
    }
};


#endif /* SCREEN_ANALIZER_HPP_ */
