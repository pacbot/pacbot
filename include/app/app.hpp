/*
 * app.hpp
 *
 *  Created on: 25/11/2014
 *      Author: Dani
 */

#ifndef APP_HPP_
#define APP_HPP_

#include "global.hpp"
#include "app/screen_analizer.hpp"
#include "app/view.hpp"
#include "app/input_interface.hpp"
#include "app/neural_manager.hpp"
#include "app/perception_manager.hpp"
#include "app/training_manager.hpp"
#include "app/genetic_trainer.hpp"
#include "game/pacman.hpp"
#include "game/ghost.hpp"
#include "game/grid.hpp"

enum AppOptions {RUN_BOT = 0, TRAIN_GENETIC, TRAIN_SUPERVISED, TRAIN_NET};
typedef std::vector<std::string> arguments_t;

/**
 * Aplicaci�n principal
 *
 * Clase para representar la aplicaci�n principal del bot (interfaz de usuario).
 */
class App {
  public:
    static const int ERROR_CODE   = -1; /**< C�digo de error */
    static const int SUCCESS_CODE =  1; /**< C�digo OK */

    static constexpr char* TITLE = "PacBot 1.0.0"; /**< T�tulo de la app */ // TODO: Cableado

  private:
    // "Constantes"
    int  START_GAME_DELAY;   /**< Delay para pulsar el botón de iniciar partida hasta que se reinicia. */
    int  START_DELAY;        /**< Retardo desde que se da al botón de inicio hasta que empieza la partida. */
    WORD RESET_KEY;          /**< Tecla de reinicio del juego. */
    int  RESET_BUTTON_REL_X; /**< Posición x del botón de reinicio relativa al área de análisis. */
    int  RESET_BUTTON_REL_Y; /**< Posición y del botón de reinicio relativa al área de análisis. */
    int  PLAY_BUTTON_REL_X;  /**< Posición x del botón de inicio relativa al área de análisis. */
    int  PLAY_BUTTON_REL_Y;  /**< Posición y del botón de inicio relativa al área de análisis. */


    // Variables de clase
    bool m_running; /**< Ejecutando aplicación. */
    bool m_paused;  /**< Saber si el bot está pausado. */

    arguments_t m_arguments;

    AppOptions m_appOption;

    int m_iteracion;

    SDL_Event event; /**< Estructura de eventos SDL2 */

    ScreenAnalizer* m_screen_analizer; /**< Analizador de pantalla. */
    InputInterface* m_input; /**< Emulaci�n de la entrada. */
    GameView* m_gameview; /**< Ventana para ver datos del juego. */
    NeuralManager* m_neural_manager; /**< Controlador de la red neuronal */
    PerceptionManager* m_perception_manager; /** Controlador de las percepciones del tablero */
    TrainingManager* m_training_manager; /** Controlador del entrenamiento supervisado */
    GeneticTrainer* m_genetic_trainer; /** Controlador del algoritmo genético */

    Pacman m_pacman; /**< Jugador (pacman). */
    Ghost  m_blinky_red;  /**< Fantasma rojo. */
    Ghost  m_pinky_pink;  /**< Fantasma rosa. */
    Ghost  m_inky_blue;   /**< Fantasma azul. */
    Ghost  m_clyde_orange; /**< Fantasma naranja. */

    Grid   m_grid; /**< Rejilla para traducir de p�xeles a posiciones */;

  private:
    /** Constructor privado. */
    App();

    /** Funci�n de iniciaci�n.
     * @return true si arranca bien, false en caso contrario. */
    bool onInit();

    /** Funci�n de cierre.
     * @return true si cierra bien, false en caso contrario. */
    bool onClose();

    /** Funci�n de eventos (teclado, rat�n, ...). */
    void onEvent();
    /** Funci�n de actualizaci�n. */
    void onLoop();
    /** Funci�n de renderizado. */
    void onRender();

    /** Qu� hacer cuando se detecta un game over. */
    void onGameOver();
      /** Reiniciar partida. */
      void reset_game();
      /** Iniciar nueva partida. */
      void start_game();
  public:
    /** Destructor */
    ~App();

    /** Singleton class.
     * @return Instancia �nica de la clase. */
    static App& getInstance();

    /** Funci�n de ejecuci�n.
     * @param opt Tipo de ejeción de la app.
     * @param args argumentos de los distintos tipos de ejeucion
     * @return Retorna el estado de la aplicaci�n (-1 en caso de error). */
    int onExecute(AppOptions opt, arguments_t args);

    /** Parar ejecuci�n */
    void exit();

    /** Comprobar si el bot est� pausado */
    inline bool isPaused() {
      return m_paused;
    }

    /** Getter del tablero o rejilla. */
    inline Grid& getGrid() {
      return m_grid;
    }

    /** Getter de la opciónd e la app */
    inline AppOptions getAppOption() {
      return m_appOption;
    }

  // --------------------------------------------------------------------------- //
  //                                 UTILIDADES                                  //
  // --------------------------------------------------------------------------- //
  public:
    /** Getter de pacman
     * @return Puntero a objeto de tipo Pacman.*/
    inline Pacman* getPacman() {
      return &m_pacman;
    }

    /** Getter de fantasma seg�n tipo
     * @param type Tipo de fantasma: BLINKY, PINKY, INKY o CLYDE
     * @return Puntero a objeto de tipo Ghost.*/
    inline Ghost* getGhost(ghost_types_t type) {
      switch(type) {
        case BLINKY: return &m_blinky_red;   break;
        case PINKY:  return &m_pinky_pink;   break;
        case INKY:   return &m_inky_blue;    break;
        case CLYDE:  return &m_clyde_orange; break;
      }
      return NULL;
    }

    /** Getter de posici�n del pacman.
     * @param x Posici�n X en el tablero.
     * @param y Posici�n Y en el tablero. */
    inline void getPacmanPos(int& x, int& y) {
      return m_pacman.getGridPos(m_grid, x, y);
    }

    /** Getter de posici�n de alg�n fantasma.
     * @param type Tipo de fantasma: BLINKY, PINKY, INKY o CLYDE
     * @param x Posici�n X en el tablero.
     * @param y Posici�n Y en el tablero. */
    inline void getGhostPos(ghost_types_t type, int& x, int& y) {
      return getGhost(type)->getGridPos(m_grid, x, y);
    }

  // --------------------------------------------------------------------------- //
  //                                 PERCEPCIONES                                //
  // --------------------------------------------------------------------------- //
  public:
    const double GHOST_NOT_DETECTED   = 0.5; /**< Fantasma no detectado. */
    const double GHOST_VULNERABLE     = 1.0; /**< Fantasma vulnerable (percepci�n). */
    const double GHOST_NOT_VULNERABLE = 0.0; /**< Fantasma no vulnerable (percepci�n). */

    /** PERCEPCION: Getter del estado de vulnerabilidad de un fantasma.
     * @param type Tipo de fantasma: BLINKY, PINKY, INKY o CLYDE
     * @return GHOST_NOT_DETECTED si el fantasma no ha sido detectado. GHOST_VULNERABLE si el fantasma "type" es vulnerable. GHOST_NOT_VULNERABLE en cualquier otro caso. */
    inline double perception_ghostIsVulnerable(ghost_types_t type) {
      Ghost* g = getGhost(type);
      if     (!g->detected()   ) return GHOST_NOT_DETECTED;
      else if( g->vulnerable() ) return GHOST_VULNERABLE;
      else                       return GHOST_NOT_VULNERABLE;
    }
};



#endif /* APP_HPP_ */
