/*
 * perception_manager.hpp
 *
 *  Created on: 12/01/2015
 *      Author: Cristo
 */

#ifndef PERCEPTION_MANAGER_HPP_
#define PERCEPTION_MANAGER_HPP_

#include "map/MapaGrafo.hpp"
#include "neural_net/globalNN.hpp"
#include "game/pacman.hpp"
#include "game/ghost.hpp"

class PerceptionManager {

  private:

    MapaGrafo* m_map;

    bool m_inited;

    Pacman* m_pacman; /**< Un pacman (jugador). Referencia externa. */
    Ghost*  m_blinky_red;   /**< Fantasma rojo. */
    Ghost*  m_pinky_pink;   /**< Fantasma rosa. */
    Ghost*  m_inky_blue;    /**< Fantasma azul. */
    Ghost*  m_clyde_orange; /**< Fantasma naranja. */
    Grid* m_grid; /**< Tablero. */

    neuron_t* m_perceptions;

  public:
    /** Constructor.
     * @param objects Lista de objetos del juego. */
    PerceptionManager(std::vector<Object*> objects);

    /** Cálculo de las percepciones. */
    void onLoop();

    /** Cálculo de las percepciones. */
    void onClose();

    /** Obtención de las percepciones. */
    neuron_t* getPerceptions() { return m_perceptions; }

    neuron_t* getCopyOfPerceptions() {
      neuron_t* input = new neuron_t[N_NEURONS[0]];
      for (int i = 0; i < N_NEURONS[0]; i++)
        input[i] = m_perceptions[i];
      return input;
    }

    /* Obtención de la puntuación */
    int getPuntuacion() { return m_map->getPuntuacion(); }

    /** Resetear el tablero. */
    void resetGame() { m_map->initGame(); }
};



#endif /* PERCEPTION_MANAGER_HPP_ */
