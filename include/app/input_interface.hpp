/*
 * input_interface.hpp
 *
 *  Created on: 10/12/2014
 *      Author: Dani
 */

#ifndef INPUT_INTERFACE_HPP_
#define INPUT_INTERFACE_HPP_

#include "global.hpp"
#include "app/perception_manager.hpp"
#include "app/screen_analizer.hpp"

/** Movimientos posibles. */
enum Movements {
    UP = 0 /**< Arriba.    */,
    DOWN  /**< Abajo.     */,
    RIGHT /**< Derecha.   */,
    LEFT  /**< Izquierda. */,
    NONE  /**< Ninguno.    */
};

const extern char* MOVEMENTS_STR[];

/**
 * @brief Interfaz de movimiento.
 *
 * Clase para definir la interfaz de movimiento del bot (para que pueda "pulsar" las teclas virtualmente).
 */
class InputInterface {
  private:

    static const int IT_MOVE = 5;

    PerceptionManager* m_perception_manager;

    // Movimientos realizados.
    Movements m_last_move;    /**< Último movimiento realizado. */
    Movements m_current_move; /**< Movimiento a realizar en la iteraci�n actual. */

    Movements m_last_key_pressed; /**< Última tecla pulsada por el usuario. */

    bool m_available_movements[4];

    // Acciones del comecocos ("Constantes").
    WORD m_vk_up;    /**< Tecla para moverse en la dirección Movements::UP. Véase http://msdn.microsoft.com/en-us/library/windows/desktop/dd375731%28v=vs.85%29.aspx */
    WORD m_vk_left;  /**< Tecla para moverse en la dirección Movements::LEFT. Véase http://msdn.microsoft.com/en-us/library/windows/desktop/dd375731%28v=vs.85%29.aspx */
    WORD m_vk_down;  /**< Tecla para moverse en la dirección Movements::DOWN. Véase http://msdn.microsoft.com/en-us/library/windows/desktop/dd375731%28v=vs.85%29.aspx */
    WORD m_vk_right; /**< Tecla para moverse en la dirección Movements::RIGHT. Véase http://msdn.microsoft.com/en-us/library/windows/desktop/dd375731%28v=vs.85%29.aspx */

    bool m_supervised;   /**< Variable booleana para determinar si la interfaz está pausada (sólo leer) o no (moverse automáticamente). */

    int m_iteracion;

  public:
    /** Constructor. Inicializa variables, entre otras cosas. */
    InputInterface(PerceptionManager*);

    /** Determinar el próximo movimiento.
     * @param m Movimiento a realizar. */
    void move(Movements m = Movements::NONE);

    /** Obtener el último movimiento ejecutado.
     * @return último movimiento ejecutado, de tipo Movements. */
    Movements getLastMove() { return m_last_move;    };

    /** Obtener el movimiento a ejecutar.
     * @return Movimiento a ejecutar, de tipo Movements. */
    Movements getMove()     { return m_current_move; };

    // ...

    /** Eventos. */
    void onEvent(SDL_Event* event);

    /** Renderizado. */
    void onLoop();

    /** Aplicar movimiento (pulsar tecla). */
    void onRender(); // mostrar direccion en algun punto de la pantalla, o algo

    /** Pausar Resumir ejecución del módulo. */
    inline void set_supervised()     { m_supervised = true; };

    /** Resumir ejecución del módulo. */
    inline void set_unsupervised()    { m_supervised = false; };

    /** Getter de m_paused.
     * @return true si el módulo esta pausado; false en caso contrario.*/
    inline bool is_supervised() { return m_supervised; };

    /** Getter de la última tecla pulsada.
     * @return última tecla pulsada, de tipo Movements. */
    inline Movements getLastKeyPressed() {
      return m_last_key_pressed;
    }

    /** Indica si es su turno de moverse */
    bool mustMove();

    /** Clicar en la pantalla en una posición relativa al área de análisis.
     * @param s Analizador de pantalla.
     * @param x Posición x relativa al analizador.
     * @param y Posición y relativa al analizador. */
    void click_relative(ScreenAnalizer* s, int x, int y);
    /** Clcar en la pantalla en una posición.
     * @param x Posición x de la pantalla.
     * @param y Posición y de la pantalla. */
    void click(int x, int y);

    /**Pulsar tecla.
     * @param vk Código de tecla a pulsar (http://msdn.microsoft.com/es-es/library/windows/desktop/dd375731%28v=vs.85%29.aspx)*/
    void press_key(WORD vk);
};



#endif /* INPUT_INTERFACE_HPP_ */
