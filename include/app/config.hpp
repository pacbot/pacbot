/*
 * config.hpp
 *
 *  Created on: 30/12/2014
 *      Author: Dani
 */

#ifndef CONFIG_HPP_
#define CONFIG_HPP_

#include "global.hpp"

#include <inih/INIReader.h>
#include <climits>

/**
 * Clase para gestionar la configuraci�n de la aplicaci�n.
 *
 * Para el fichero ini
 *
 @code
 [Seccion1]
   clave1 = 0
   clave2 = 2.0
   clave3 = 255 0 0

 [Seccion2]
   clave1 = "clave1"
   clave2 = true
 @endcode
 *
 * Las variables se guardan en un mapa con la siguiente estructura:
 *
 * Fichero INI
 *  |- Seccion1
 *  |  |- clave1->0
 *  |  |- clave2->2.0
 *  |  |- clave3->color(255, 0, 0)
 *  |
 *  |- Seccion2
 *     |- clave1->Valor
 *     |- clave2->true
 *
 * Cualquier clave puede ser recuperada como cualquier tipo de valor (String, Int, Double, Bool o Color). Por tanto,
 * se debe saber a priori el tipo de dato a recuperar.
 */
class Config {
  public:
    const char* CONFIG_FILE = "assets/config.ini"; /**< Fichero de configuraci�n. */

    const std::string DEFAULT_STR       = "\0";    /**< String por defecto a recuperar. */
    const int         DEFAULT_INT       = INT_MAX; /**< Int por defecto a recuperar. */
    const double      DEFAULT_DOUBLE    = 0.0;     /**< Double por defecto a recuperar. */
    const bool        DEFAULT_BOOL      = false;   /**< Bool por defecto a recuperar. */
    const std::string DEFAULT_COLOR_STR = "0 0 0"; /**< Color (str) por defecto a recuperar. */

  private:
    INIReader m_reader; /**< Lector y parseador de ficheros INI (librer�a). */

    /**< Constructor: carga las variables de "CONFIG_FILE" (privado, singleton class). */
    Config();
    /**< Destructor principal. */
    ~Config();

  public:
    /**< Instanciador (singleton class): obtiene una copia de la �nica instance de la clase Config. */
    static Config& getInstance();

    /** Obtener valor "string" dada una secci�n y una clave.
     * @param section Secci�n a buscar
     * @param name    Nombre de la variable a buscar en la secci�n.
     * @return        Valor de dicha clave (string). Retorna DEFAULT_STR si no lo encuentra. */
    std::string get_str   (std::string section, std::string name);

    /** Obtener valor "int" dada una secci�n y una clave.
     * @param section Secci�n a buscar
     * @param name    Nombre de la variable a buscar en la secci�n.
     * @return        Valor de dicha clave (int). Retorna DEFAULT_INT si no lo encuentra. */
    int         get_int   (std::string section, std::string name);

    /** Obtener valor "double" dada una secci�n y una clave.
     * @param section Secci�n a buscar
     * @param name    Nombre de la variable a buscar en la secci�n.
     * @return        Valor de dicha clave (double). Retorna DEFAULT_DOUBLE si no lo encuentra. */
    double      get_double(std::string section, std::string name);

    /** Obtener valor "bool" dada una secci�n y una clave.
     * @param section Secci�n a buscar
     * @param name    Nombre de la variable a buscar en la secci�n.
     * @return        Valor de dicha clave (bool). Retorna DEFAULT_BOOL si no lo encuentra. */
    bool        get_bool  (std::string section, std::string name);

    /** Obtener valor "color_t" dada una secci�n y una clave.
     * @param section Secci�n a buscar
     * @param name    Nombre de la variable a buscar en la secci�n.
     * @return        Valor de dicha clave (color_t). Retorna DEFAULT_COLOR_STR (como una estructura color_t) si no lo encuentra. */
    color_t     get_color (std::string section, std::string name);
};



#endif /* CONFIG_HPP_ */
