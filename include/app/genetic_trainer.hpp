/*
 * genetic_trainer.hpp
 *
 *  Created on: 17/01/2015
 *      Author: Cristo
 */

#ifndef GENETIC_TRAINER_HPP_
#define GENETIC_TRAINER_HPP_

#include "neural_net/MultiLayerPerceptron.hpp"
#include "neural_net/WeightMatrix.hpp"
#include "app/perception_manager.hpp"
#include "app/neural_manager.hpp"
#include "global.hpp"

class GeneticTrainer {

  private:

    int N_POBLACION;
    int N_GENERACIONES;
    int N_SELECCIONADOS;
    int N_MIN_MUTACIONES, N_MAX_MUTACIONES;
    int N_JUEGOS_BOT;

    bool m_inited;

    PerceptionManager* m_perception_manager;
    NeuralManager* m_neural_manager;

    MultiLayerPerceptron** m_poblacion;

    int m_generacion;
    int m_paisano;
    int m_juego_paisano;
    int* m_beneficios;

  public:
    /** Constructor.
     * @param objects Lista de objetos del juego. */
    GeneticTrainer(PerceptionManager* p_m, NeuralManager* n_m);

    /** Iniciar manager. */
    bool onInit(int, int, int, int, int, int);

    /** Función de iteración. */
    void onLoop();

    /** Función de cierre. */
    void onClose();


    /** Algoritmo Genético */
    void avanzarGeneracion();
    void exportPoblacion();
    void initGame();
    void endGame();

    WeightMatrix*** mergeNets(MultiLayerPerceptron* padre, MultiLayerPerceptron* madre);
};



#endif /* GENETIC_TRAINER_HPP_ */
