/*
 * global.hpp
 *
 *  Created on: 25/11/2014
 *      Author: Dani
 */

#ifndef GLOBAL_HPP_
#define GLOBAL_HPP_

#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>

#include <rapidxml-1.13/rapidxml.hpp>
#include <rapidxml-1.13/rapidxml_print.hpp>

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <SDL2/SDL_ttf.h>

#define SSTR(x) dynamic_cast<std::ostringstream &>((std::ostringstream() << std::dec << x)).str()

inline int randBetween(int first, int second) {

  return (rand() % (second - first + 1)) + first;
}

/** Typedef para definir posiciones en pantalla. */
typedef double position_t;
/** Typedef para definir un byte. */
typedef unsigned char byte;

/** struct para rect�ngulos. */
template <class T> struct rectangle_t {
  T x;      /**< Posici�n y. */
  T y;      /**< Posici�n x. */
  T width;  /**< Ancho. */
  T height; /**< Alto. */
};

/** Typedef para rect�ngulos con valores enteros. */
typedef rectangle_t<int> rect_t;

/** struct para definir colores */
struct color_t {
  int r; /**< Rojo. */
  int g; /**< Verde. */
  int b; /**< Azul. */

  /** Comparador.
   *
   * Compara si el color es similar a c, componente a componente, con un porcentaje de error permitido de error_percent.
   * @param c Color comparado.
   * @param error_percent Error permitido.
   * @return true si son iguales, false en caso contrario.
   */
  bool similar(color_t c, float error_percent = 0.05) const {
    if(c.r > r * (1.f + error_percent) or c.r < r * (1.f - error_percent))
      return false;
    if(c.b > b * (1.f + error_percent) or c.b < b * (1.f - error_percent))
      return false;
    if(c.g > g * (1.f + error_percent) or c.g < g * (1.f - error_percent))
      return false;

    return true;
  }

  /** Parseador
   *
   * La cadena de entrada debe ser de la forma "<r> <g> <b>", por ejemplo, "205 128 55".
   * @param color Cadena que representa el color. */
  void parse(std::string str_color) {
    std::stringstream ss(str_color);
    ss >> r >> g >> b;
  }
};

#endif /* GLOBAL_HPP_ */
