/*
 * pacman.hpp
 *
 *  Created on: 25/11/2014
 *      Author: Dani
 */

#ifndef PACMAN_HPP_
#define PACMAN_HPP_

#include "game/object.hpp"

/** @brief Clase que representa al comecocos.
 *
 * Similar (id�ntico) a Object, variando s�lo valores de las variables miembro.
 */
class Pacman: public Object {
  private:
    // ...

  public:
    /** Constructor. */
    Pacman();
    /** Destructor. */
    virtual ~Pacman();
};



#endif /* PACMAN_HPP_ */
