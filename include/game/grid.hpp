/*
 * grid.hpp
 *
 *  Created on: 07/12/2014
 *      Author: Dani
 */

#ifndef GRID_HPP_
#define GRID_HPP_

#include "game/object.hpp"

/** Rejilla o tablero del juego. */
class Grid: public Object {
  private:
    const color_t GRID_COLOR = {128, 128, 128}; /**< Color con el que pintar el tabler (gris). */

    float CELL_WIDTH;  /**< Ancho aproximado de cada celda, en p�xeles: aproximadamente, es la distancia promedio en el eje x entre 2 cocos. */
    float CELL_HEIGHT; /**< Alto aproximado de cada celda, en p�xeles: aproximadamente, es la distancia promedio en el eje y entre 2 cocos. */

    float START_X;     /**< Posici�n inicial del tablero en el eje horizontal: es la "pared" invisible de la que parten todas las filas de celdas. */
    float START_Y;     /**< Posici�n inicial del tablero en el eje vertical: es la "pared" invisible de la que parten todas las columnas de celdas. */

    float COLUMNS;     /**< N�mero de columnas; cantidad de celdas en el eje horizontal. */
    float ROWS;        /**< N�mero de filas; cantidad de celdas en el eje vertical. */

  public:
    /** Constructor. */
    Grid();

    /** Renderizado. */
    virtual void onRender();

    /** @brief Mapeador de posiciones.
     *
     * Mapea una posici�n de pantalla a una casilla del tablero.
     * @param in_x X de entrada.
     * @param in_y Y de entrada.
     * @param out_x X de salida.
     * @param out_y Y de salida. */
    void map(float in_x, float in_y, int& out_x, int& out_y);
    /** @brief Mapeador de posiciones.
     * @param in_x X de entrada.
     * @return X de salida. */
    int mapX(float in_x);
    /** @brief Mapeador de posiciones.
     * @param in_y Y de entrada.
     * @return Y de salida. */
    int mapY(float in_y);

  protected:
    /** Getter de la posici�n final de la rejilla en el eje X.
     * @return Posici�n final de la rejilla (p�xeles) en el eje X. */
    inline float get_endX() {
      return START_X + COLUMNS * CELL_WIDTH;
    }

    /** Getter de la posici�n final de la rejilla en el eje Y.
     * @return Posici�n final de la rejilla (p�xeles) en el eje Y. */
    inline float get_endY() {
      return START_Y + ROWS * CELL_HEIGHT;
    }
};



#endif /* GRID_HPP_ */
