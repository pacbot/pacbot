/*
 * object.hpp
 *
 *  Created on: 25/11/2014
 *      Author: Dani
 */

#ifndef OBJECT_HPP_
#define OBJECT_HPP_

#include <windows.h>

#include "global.hpp"

class Grid; // Fordward declaration

/**
 * Clase abstracta que representa un objeto del juego.
 */
class Object {
  protected:
    // Constantes
    const int DETECTED = 1;         /**< Objeto detectado. */
    const int ALREADY_DETECTED = 0; /**< Objeto ya detectado. */
    const int NOT_DETECTED = -1;    /**< Objeto sin detectar. */

  protected:
    // "Constantes"
    int     COLOR_SEARCH_WIDTH;       /**< Ancho de la imagen a buscar. */
    int     COLOR_SEARCH_HEIGHT;      /**< Alto de la imagen a buscar. */
    int     COLOR_SEARCH_INC;         /**< Incremento en la b�squeda (1) en el eje X e Y. */
    float   COLOR_SEARCH_PERCENT;     /**< Cantidad de p�xeles de color encontrado. */
    float   COLOR_SEARCH_OFFSET_X;    /**< Factor de ajuste del eje X. */
    float   COLOR_SEARCH_OFFSET_Y;    /**< Factor de ajuste del eje Y. */

    float   COLOR_ERROR;              /**< Porcentaje de error permitido en el color. */
    color_t COLOR_DETECT;             /**< Color a buscar. */

  protected:
    position_t m_x; /**< Posici�n X del objeto. */
    position_t m_y; /**< Posici�n Y del objeto. */
    // ...

    bool m_detected; /**< Marcado como detectado. */

  public:
    /** Constructor. */
    Object();
    /** Destructor. */
    virtual ~Object();

    position_t getX(); /**< Getter posici�n X.*/
    position_t getY(); /**< Getter posici�n Y.*/

    void setX(position_t x); /**< Setter posici�n X.*/
    void setY(position_t y); /**< Setter posici�n Y.*/
    // ...

    /** Analizar si los p�xeles cercanos conforman el objeto del juego.
     * @param pixels Array de p�xeles a analizar
     * @param width Ancho de pixels.
     * @param height Alto de pixels.
     * @param x Posici�n x en la que buscar.
     * @param y Posici�n y en la que buscar.
     * @return Puede retornar DETECTED, ALREADY_DETECTED y NOT_DETECTED.
     */
    virtual int detect(BYTE *pixels, int width, int height, int x, int y);

    /** Objeto detectado en pantalla.
     * @return true si ha sido detectado en pantalla. false en caso contrario.*/
    virtual bool detected();
    /** Marcador de detecci�n (true o false).
     * @param value Nuevo valor de m_detected.*/
    virtual void mark_detected(bool value);

    /** Renderizar. */
    virtual void onRender();

    /** Obtener posici�n x, y en el tablero o rejilla
     * @param g Referencia a rejilla o tablero.
     * @param x Referencia a "x" de salida.
     * @param y Referencia a "y" de salida. */
    virtual void getGridPos(Grid& g, int& x, int& y);
    /** Obtener posici�n x en el tablero o rejilla
     * @param g Referencia a rejilla o tablero.
     * @return Posici�n x en la rejilla. */
    virtual int getGridX(Grid& g);
    /** Obtener posici�n y en el tablero o rejilla
     * @param g Referencia a rejilla o tablero.
     * @return Posici�n y en la rejilla. */
    virtual int getGridY(Grid& g);
};

#endif /* OBJECT_HPP_ */
