/*
 * pacman.hpp
 *
 *  Created on: 25/11/2014
 *      Author: Dani
 */

#ifndef GHOST_HPP_
#define GHOST_HPP_

#include "game/object.hpp"

/** Enum de los distintos tipos de fantasmas */
enum ghost_types_t {
  BLINKY /**< BLINKY (rojo) */,
  PINKY  /**< PINKY (rosa) */,
  INKY   /**< INKY (azul) */,
  CLYDE  /**< CLYDE (naranja) */
};

/** Clase que representa a un fantasma en el juego (cualquier de ellos). */
class Ghost: public Object {
  private:
    double VULNERABLE_MAX_DISTANCE;        /**< Distancia m�xima de b�squeda cuando el fantasma es vulnerable. */

    color_t VULNERABLE_COLOR_DETECT_1;     /**< Color de detecci�n (1) cuando el fantasma es vulnerable. */
    color_t VULNERABLE_COLOR_DETECT_2;     /**< Color de detecci�n (2) cuando el fantasma es vulnerable. */

    float VULNERABLE_COLOR_SEARCH_PERCENT; /**< Porcentaje de aciertos en el recuadro de b�squeda para considerar el fantasma como "detectado". */
    float VULNERABLE_COLOR_ERROR;          /**< Error permitido (en %) en la comparaci�n de colores cuando el fantasma es vulnerable. */

    float VULNERABLE_OFFSET_X;             /**< Offset de b�squeda en el eje X cuando el fantasma es vulnerable. */
    float VULNERABLE_OFFSET_Y;             /**< Offset de b�squeda en el eje Y cuando el fantasma es vulnerable. */

    int VULNERABLE_SEARCH_WIDTH;           /**< Ancho de b�squeda (tama�o del fantasma) cuando el fantasma es vulnerable. */
    int VULNERABLE_SEARCH_HEIGHT;          /**< Alto de b�squeda (tama�o del fantasma) cuando el fantasma es vulnerable. */

    bool m_vulnerable;                     /**< Flag para saber si el fantasma es vulnerable o no. */
    ghost_types_t m_type;                  /**< Tipo de fantasma, listado en ghost_types_t. */

  public:
    /** Constructor.
     * @param type Tipo de fantasma a crear.*/
    Ghost(ghost_types_t type);
    /** Destructor. */
    virtual ~Ghost();

    /** Detector de fantasma vulnerable.
     * @param pixels Array de p�xeles a analizar
     * @param width Ancho de pixels.
     * @param height Alto de pixels.
     * @param x Posici�n x en la que buscar.
     * @param y Posici�n y en la que buscar.
     * @return Puede retornar DETECTED, ALREADY_DETECTED y NOT_DETECTED.
     */
    int vulnerable_detect(BYTE *pixels, int width, int height, int x, int y);

    /** Renderizado. */
    virtual void onRender();

    /** Getter de m_vulnerable.
     * @return true si el fantasma es vulnerable. false en caso contrario. */
    bool vulnerable();

    /** Modificador de m_vulnerable.
     * @param value Nuevo valor de m_vulnerable. */
    void mark_vulnerable(bool value);

    /** Getter del tipo de fantasma.
     * @return Tipo de fantasma. */
    inline ghost_types_t getType() {
      return m_type;
    }
};



#endif /* GHOST_HPP_ */
